/*
	Copyright (c) 2007, 2008 Jacob Beller, Hanno Braun, Jan Lücker
	
	Permission to use, copy, modify, and/or distribute this software for any
	purpose with or without fee is hereby granted, provided that the above
	copyright notice and this permission notice appear in all copies.

	THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
	WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
	MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
	ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
	WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
	ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
	OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*/



package de.elrador.treibjagd.common.network.ingame;



import de.elrador.treibjagd.common.game.cards.EffectCard;

import java.io.Serializable;



/**
 * Used to notify the client that a card in the player's hand has changed.
 */

public class ChangeCardInHandMessage implements Serializable {
	
	public byte slotId; // which slot in the hand; 0 - 3
	public byte cardId; // id number of the card
	
	
	
	public ChangeCardInHandMessage() {}
	public ChangeCardInHandMessage(byte theSlotId, EffectCard card) {
		slotId = theSlotId;
		cardId = card.id();
	}
}
