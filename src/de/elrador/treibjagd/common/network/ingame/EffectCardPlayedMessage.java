/*
	Copyright (c) 2007, 2008 Jacob Beller, Hanno Braun, Jan Lücker
	
	Permission to use, copy, modify, and/or distribute this software for any
	purpose with or without fee is hereby granted, provided that the above
	copyright notice and this permission notice appear in all copies.

	THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
	WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
	MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
	ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
	WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
	ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
	OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*/



package de.elrador.treibjagd.common.network.ingame;



import de.elrador.treibjagd.common.game.cards.EffectCard;

import java.io.Serializable;



/**
 * Broadcast by the server if a client has played an effect card.
 */

public class EffectCardPlayedMessage implements Serializable {
	
	public byte cardId;
	private byte[] xTargets;
	private byte[] yTargets;
	
	public EffectCardPlayedMessage() {}
	public EffectCardPlayedMessage(EffectCard card, EffectCard.Target... targets) {
		cardId = card.id();
		
		xTargets = new byte[targets.length];
		yTargets = new byte[targets.length];
		
		byte i = 0;
		for (EffectCard.Target target : targets) {
			xTargets[i] = target.x;
			yTargets[i] = target.y;
			i++;
		}
	}
	
	
	
	/**
	 * Returns the targets of the effect card.
	 */
	
	public EffectCard.Target[] targets() {
		EffectCard.Target[] targets = new EffectCard.Target[xTargets.length];
		for (byte i = 0; i < xTargets.length; i++) {
			targets[i] = new EffectCard.Target(xTargets[i], yTargets[i]);
		}
		return targets;
	}
}
