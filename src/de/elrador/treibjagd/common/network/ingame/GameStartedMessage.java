/*
	Copyright (c) 2007, 2008 Jacob Beller, Hanno Braun, Jan Lücker
	
	Permission to use, copy, modify, and/or distribute this software for any
	purpose with or without fee is hereby granted, provided that the above
	copyright notice and this permission notice appear in all copies.

	THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
	WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
	MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
	ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
	WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
	ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
	OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*/



package de.elrador.treibjagd.common.network.ingame;



import de.elrador.treibjagd.common.game.Player;
import de.elrador.treibjagd.common.game.Race;

import java.io.Serializable;



public class GameStartedMessage implements Serializable {
	
	public byte id;
	
	// The framework doesn't yet support arrays as attribute types, so we have to hack something together
	// until it does.
	private byte playerNumber;
	private String playerName0;
	private byte playerRace0;
	private String playerName1;
	private byte playerRace1;
	private String playerName2;
	private byte playerRace2;
	private String playerName3;
	private byte playerRace3;
	private String playerName4;
	private byte playerRace4;
	private String playerName5;
	private byte playerRace5;
	
	// Seed for play area generation.
	public long seed;
	
	
	
	public GameStartedMessage() {}
	public GameStartedMessage(byte theId, Player[] players, long theSeed) {
		id = theId;
		seed = theSeed;
		
		playerNumber = (byte)players.length;
		
		switch (playerNumber) {
			case 6: playerName5 = players[5].name();
			case 5: playerName4 = players[4].name();
			case 4: playerName3 = players[3].name();
			case 3: playerName2 = players[2].name();
			case 2: playerName1 = players[1].name();
			case 1: playerName0 = players[0].name();
		}
		
		switch (playerNumber) {
			case 6: playerRace5 = players[5].race().networkId();
			case 5: playerRace4 = players[4].race().networkId();
			case 4: playerRace3 = players[3].race().networkId();
			case 3: playerRace2 = players[2].race().networkId();
			case 2: playerRace1 = players[1].race().networkId();
			case 1: playerRace0 = players[0].race().networkId();
		}
	}
	
	
	
	/**
	 * Returns the players.
	 */
	
	public Player[] players() {
		Player[] players = new Player[playerNumber];
		switch (players.length) {
			case 6: players[5] = new Player((byte)5, playerName5, Race.raceFromByte(playerRace5));
			case 5: players[4] = new Player((byte)4, playerName4, Race.raceFromByte(playerRace4));
			case 4: players[3] = new Player((byte)3, playerName3, Race.raceFromByte(playerRace3));
			case 3: players[2] = new Player((byte)2, playerName2, Race.raceFromByte(playerRace2));
			case 2: players[1] = new Player((byte)1, playerName1, Race.raceFromByte(playerRace1));
			case 1: players[0] = new Player((byte)0, playerName0, Race.raceFromByte(playerRace0));
		}
		return players;
	}
}
