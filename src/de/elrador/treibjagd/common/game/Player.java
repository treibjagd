/*
	Copyright (c) 2007, 2008 Jacob Beller, Hanno Braun, Jan Lücker
	
	Permission to use, copy, modify, and/or distribute this software for any
	purpose with or without fee is hereby granted, provided that the above
	copyright notice and this permission notice appear in all copies.

	THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
	WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
	MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
	ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
	WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
	ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
	OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*/



package de.elrador.treibjagd.common.game;



/**
 * Models a player. Contains all player-related data that is shared between client and server.
 */

public class Player implements Comparable<Player>, GameConstants {
	
	private byte id;
	private String name;
	private Race race;
	
	private int score;
	private int animalsLeft;
	
	
	
	/**
	 * Initializes the player's data.
	 */
	
	public Player(byte theId, String theName, Race theRace) {
		id = theId;
		name = theName;
		race = theRace;
		
		score = 0;
		animalsLeft = STARTINGSTACK_ANIMALS_PER_PLAYER + REINFORCEMENTSTACK_ANIMALS_PER_PLAYER;
	}
	
	
	
	/**
	 * Returns the player's id.
	 */
	
	public byte id() {
		return id;
	}
	
	
	
	/**
	 * Returns the player's name.
	 */
	
	public String name() {
		return name;
	}
	
	
	
	/**
	 * Returns the player's race.
	 */
	
	public Race race() {
		return race;
	}
	
	
	
	/**
	 * Returns the player's color id.
	 */
	
	public String colorId() {
		switch (id) {
			case 0: return "gr";
			case 1: return "hb";
			case 2: return "ok";
			case 3: return "pi";
			case 4: return "ro";
			case 5: return "we";
			default: throw new AssertionError("Invalid player id: " + id);
		}
	}
	
	
	
	/**
	 * Returns the player's current score.
	 */
	
	public int score() {
		return score;
	}
	
	
	
	/**
	 * Returns the number of animals this player has left.
	 */
	
	public int animalsLeft() {
		return animalsLeft;
	}
	
	
	
	/**
	 * Increases the score.
	 */
	
	public void increaseScore(int increment) {
		score += increment;
	}
	
	
	
	/**
	 * Decrease the number of animals.
	 */
	
	public void decreaseAnimals(int decrement) {
		animalsLeft -= decrement;
	}
	
	public int compareTo(Player o1) {
		if (score < o1.score) return -1;
		if (score > o1.score) return +1;
		return 0; // ==
	}
}
