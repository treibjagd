/*
	Copyright (c) 2007, 2008 Jacob Beller, Hanno Braun, Jan Lücker
	
	Permission to use, copy, modify, and/or distribute this software for any
	purpose with or without fee is hereby granted, provided that the above
	copyright notice and this permission notice appear in all copies.

	THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
	WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
	MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
	ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
	WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
	ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
	OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*/



package de.elrador.treibjagd.common.game;

import de.elrador.treibjagd.common.game.effects.EffectHandler;


/**
 * A card on the play field.
 */

public class FieldCard {
	
	public enum Type {
		EMPTY(0, "leer"),
		DONKEY(1, "Esel/es"),
		HORSE(2, "Pferd/pf"),
		SHEEP(3, "Schaf/sc"),
		WOLF(4, "Wolf/wo");
		
		
		
		public static Type fieldCardFromId(byte anId) {
			switch (anId) {
				case 0: return EMPTY;
				case 1: return DONKEY;
				case 2: return HORSE;
				case 3: return SHEEP;
				case 4: return WOLF;
				default: throw new IllegalArgumentException("Invalid id: " + anId);
			}
		}
		
		
		
		private byte id;
		private String pathPrefix;
		
		
		
		private Type(int theId, String theGraphicPath) {
			id = (byte)theId;
			pathPrefix = theGraphicPath;
		}
		
		
		
		/**
		 * Returns the id of the card.
		 */
		 
		public byte id() {
			return id;
		}
		
		
		
		String pathPrefix() {
			return pathPrefix;
		}
	}
	
	
	
	private Type type;
	private Player player;
	private EffectHandler effectHandler;
		
	
	
	public FieldCard(Type theType) {
		type = theType;
		player = null;
		effectHandler = new EffectHandler();
	}
	
	
	/**
	 * Returns the type.
	 */
	
	public Type type() {
		return type;
	}
	
		
	
	/**
	 * Sets the player this field card belongs to.
	 */
	
	public void player(Player aPlayer) {
		player = aPlayer;
	}
	
	
	
	/**
	 * Returns the player this card belongs to.
	 */
	
	public Player player() {
		return player;
	}
	
	public EffectHandler effectHandler() {
		return effectHandler;
	}
}
