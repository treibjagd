/*
	Copyright (c) 2007, 2008 Jacob Beller, Hanno Braun, Jan Lücker
	
	Permission to use, copy, modify, and/or distribute this software for any
	purpose with or without fee is hereby granted, provided that the above
	copyright notice and this permission notice appear in all copies.

	THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
	WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
	MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
	ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
	WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
	ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
	OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*/



package de.elrador.treibjagd.common.game;



/**
 * Enum for representing the different races. Provides methods for converting a Race into a byte for network
 * transfer and the other way around.
 */

public enum Race {
		NONE(-1, null),
		ELFEN(0, FieldCard.Type.HORSE),
		ORKS(1, FieldCard.Type.WOLF),
		MENSCHEN(2, FieldCard.Type.SHEEP),
		ZWERGE(3, FieldCard.Type.DONKEY);
		
		
		
		private byte networkId;
		private FieldCard.Type animalType;
		
		
		
		private Race(int theNetworkId, FieldCard.Type theAnimalType) {
			networkId = (byte)theNetworkId;
			animalType = theAnimalType;
		}
		
		
		
		/**
		 * Converts a byte into a race. Throws an IllegalArgumentException if the byte doesn't represent a
		 * valid race.
		 */
		
		public static Race raceFromByte(byte raceId) {
			switch (raceId) {
				case 0: return ELFEN;
				case 1: return ORKS;
				case 2: return MENSCHEN;
				case 3: return ZWERGE;
				default: throw new IllegalArgumentException("Invalid race id: " + raceId);
			}
		}
		
		
		
		/**
		 * Returns the network id (byte) of the race.
		 */
		
		public byte networkId() {
			return networkId;
		}
		
		
		
		/**
		 * Returns the animal type for this race.
		 */
		
		public FieldCard.Type animalType() {
			return animalType;
		}
	}
