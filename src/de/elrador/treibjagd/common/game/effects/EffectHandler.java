/*
 * EffectHandler.java
 *
 * Created on 13. März 2008, 23:44
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package de.elrador.treibjagd.common.game.effects;

import java.util.List;
import java.util.LinkedList;
import java.util.Iterator;

import java.awt.Graphics;
import java.awt.Image;

/**
 *
 * @author jan
 */
public class EffectHandler {
	private List<Effect> effects;
	
	/** Creates a new instance of EffectHandler */
	public EffectHandler() {
		effects = new LinkedList<Effect>();
	}
	
	public void addEffect(Effect effect) {
		effects.add(effect);
	}
	
	public void runEffects(int turnsPassed) {
		Iterator<Effect> it = effects.iterator();
		while(it.hasNext()) {
			Effect effect = it.next();
			effect.run(turnsPassed);
			if(effect.finished()) {
				it.remove();
			}
		}
	}
	
	public boolean effectPresent(Class<? extends Effect> effectClass) {
		for (Effect effect : effects) {
			if(effect.getClass() == effectClass) return true;
		}
		return false;
	}
	
	public void drawEffects(Graphics graphics) {
		for (Effect effect : effects) {
			Image image = effect.getImage();
			if(image != null)
				graphics.drawImage(image, 0, 0, null);
		}
	}
}
