/*
 * EffectPulverfass.java
 *
 * Created on 21. März 2008, 00:15
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package de.elrador.treibjagd.common.game.effects;

import de.elrador.treibjagd.common.game.PlayArea;
import de.elrador.treibjagd.common.game.FieldCard;
import de.elrador.treibjagd.common.game.Player;

import javax.imageio.ImageIO;
import java.io.IOException;
import java.io.File;

/**
 *
 * @author jan
 */
public class EffectPulverfass extends Effect {
	PlayArea area;
	FieldCard card;
	Player owner;
	
	int live;
	/** Creates a new instance of EffectPulverfass */
	public EffectPulverfass(PlayArea area_, FieldCard card_, Player owner_, int live_) {
		area = area_;
		card = card_;
		live = live_;
		owner = owner_;
		try {
			image = ImageIO.read(new File("grafik/fields/pulverfass.png"));
		} catch(IOException e) {
			e.printStackTrace();
			System.exit(1);
		}
	}
	
	public void run(int turnsPassed) {
		live -= turnsPassed;
		if(live == 0) {
			System.out.println("!");
			fin = true;
			byte[] pos = new byte[2];
			area.findCard(card, pos);
			for(byte y=0; y<3; y++) {
				area.shootCard(pos[0], y, owner);
			}
			//area.shootCard(pos[0], pos[1], owner);
		}
	}
}
