/*
	Copyright (c) 2007, 2008 Jacob Beller, Hanno Braun, Jan Lücker
	
	Permission to use, copy, modify, and/or distribute this software for any
	purpose with or without fee is hereby granted, provided that the above
	copyright notice and this permission notice appear in all copies.

	THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
	WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
	MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
	ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
	WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
	ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
	OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*/



package de.elrador.treibjagd.common.game.effects;



import de.elrador.treibjagd.common.game.FieldCard;
import de.elrador.treibjagd.common.game.PlayArea;
import de.elrador.treibjagd.common.game.Player;

import javax.imageio.ImageIO;
import java.io.IOException;
import java.io.File;



/**
 *
 * @author jan
 */

public class EffectFalle extends Effect {
	private int roundsToLive;
	Player owner;
	PlayArea area;
	byte x,y;

	public EffectFalle(PlayArea area_, byte x_, byte y_) {
		area = area_;
		x = x_;
		y = y_;
		fin = false;
		owner = area.currentTurnPlayer();
		roundsToLive = 3*area.players().length;
		try {
			image = ImageIO.read(new File("grafik/fields/trapu.png"));
		} catch(IOException e) {
			e.printStackTrace();
			System.exit(1);
		}
	}

	public void run(int turnsPassed) {
		roundsToLive -= turnsPassed;
		if(roundsToLive == 0) fin = true;
		if(area.field(x,y).card().type() != FieldCard.Type.EMPTY) {
			//field.shoot...
			area.shootCard(x, y, owner);
			fin = true;
		}
	}
}
