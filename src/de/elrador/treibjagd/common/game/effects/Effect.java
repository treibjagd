/*
 * Effect.java
 *
 * Created on 13. März 2008, 13:55
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package de.elrador.treibjagd.common.game.effects;

import java.awt.Image;

/**
 *
 * @author jan
 */
public abstract class Effect {
	protected boolean fin;
	protected Image image;
	
	public abstract void run(int turnsPassed);
	
	public boolean finished() {
		return fin;
	}
	
	public Image getImage() {
		return image;
	}
}
