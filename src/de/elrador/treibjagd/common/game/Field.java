/*
	Copyright (c) 2007, 2008 Jacob Beller, Hanno Braun, Jan Lücker
	
	Permission to use, copy, modify, and/or distribute this software for any
	purpose with or without fee is hereby granted, provided that the above
	copyright notice and this permission notice appear in all copies.

	THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
	WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
	MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
	ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
	WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
	ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
	OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*/



package de.elrador.treibjagd.common.game;

import de.elrador.treibjagd.common.game.effects.EffectHandler;
import de.elrador.treibjagd.common.game.effects.EffectFalle;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import java.awt.image.BufferedImage;
import java.awt.Image;
import javax.imageio.ImageIO;
import java.io.IOException;
import java.io.File;


/**
 * Represents a field.
 */

public class Field {
	private FieldCard card;
	private boolean illuminated;
	private EffectHandler effectHandler;
	
	public Field() {
		effectHandler = new EffectHandler();
	}
	
	/**
	 * Puts a card on this field.
	 */
	
	public void card(FieldCard aCard) {
		card = aCard;
	}
	
	
	
	/**
	 * Returns the card on this field.
	 */
	
	public FieldCard card() {
		return card;
	}
	
	
	
	/**
	 * Returns the path of the graphic that should be used to display this field.
	 */
	
	/*public String graphicPath() {
		if (card == null) {
			return "nothing.png";
		}
		
		if (card.type() != FieldCard.Type.EMPTY && card.player() == null) {
			throw new IllegalStateException("Player has not been set.");
		}
		
		StringBuilder path = new StringBuilder(30);
		
		if(effectHandler().effectPresent(EffectFalle.class)) {
			path.append("trap"); // TODO: This should be more general.
		} else {
			path.append(card.type().pathPrefix());
		}
		
		if (card.type() != FieldCard.Type.EMPTY) {
			path.append(card.player().colorId());
		}
		
		if (illuminated) {
			path.append("b");
		}
		else {
			path.append("u");
		}
		
		path.append(".png");
		
		return path.toString();
	}*/
	
	public Image getImage() {
		String pathToInitialImage;
		if(card == null || card.type() == FieldCard.Type.EMPTY) {
			pathToInitialImage = "grafik/fields/leeru.png";
		}
		else {
			pathToInitialImage = "grafik/fields/" + card.type().pathPrefix()
				+ card.player().colorId() + "u.png";
		}
		
		try {
			BufferedImage image = ImageIO.read(new File(pathToInitialImage));

			if(card == null) {
				return image;
			}

			effectHandler.drawEffects(image.getGraphics());
			card().effectHandler().drawEffects(image.getGraphics());
			
			if(illuminated) {
				BufferedImage light = ImageIO.read(new File("grafik/fields/leerb.png"));
				image.getGraphics().drawImage(light, 0, 0, null);
			}
			
			return image;
		}
		catch(IOException e) {
			e.printStackTrace();
			System.exit(1);
			return null;
		}
	}
	
	
	/**
	 * Sets the illuminated status of the card.
	 */
	
	public void illuminated(boolean isIlluminated) {
		illuminated = isIlluminated;
	}
	
	
	
	/**
	 * Returns the illumination status.
	 */
	
	public boolean illuminated() {
		return illuminated;
	}
	
	public EffectHandler effectHandler() {
		return effectHandler;
	}
}
