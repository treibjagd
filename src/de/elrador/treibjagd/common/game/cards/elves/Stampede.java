/*
	Copyright (c) 2007, 2008 Jacob Beller, Hanno Braun, Jan Lücker
	
	Permission to use, copy, modify, and/or distribute this software for any
	purpose with or without fee is hereby granted, provided that the above
	copyright notice and this permission notice appear in all copies.

	THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
	WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
	MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
	ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
	WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
	ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
	OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*/



package de.elrador.treibjagd.common.game.cards.elves;



import de.elrador.treibjagd.common.game.PlayArea;
import de.elrador.treibjagd.common.game.cards.EffectCard;
import de.elrador.treibjagd.common.game.FieldCard;



/**
 * alle eigenen Pferde 1 vor
 */

public class Stampede extends EffectCard {

	public byte id() {
		return 25;
	}

	public CardType type() {
		return CardType.ELF;
	}
	
	public int number() {
		return 2;
	}
	
	public String graphicsPath() {
		return "elf/stampede.png";
	}
	
	public void play(PlayArea area, Target... targets) {
		for(int y=0; y<3; y++) {
			int n = 5;
			for(int x=0; x<n; x++) {
				if(area.field(x, y).card() == null) continue;
				if(area.field(x, y).card().type() == FieldCard.Type.EMPTY) continue;
				if(area.field(x, y).card().player() != area.currentTurnPlayer()) continue;
				if(x == 0) n--;
				area.moveCard((byte)x, (byte)y, (byte)1);
			}
		}
	}
	
	public boolean isTargetValid(PlayArea area, Target... targets) {
		return true;
	}
	
	public byte getNumberOfTargets() {
		return 1;
	}
}
