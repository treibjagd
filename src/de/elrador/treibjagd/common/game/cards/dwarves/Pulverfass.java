/*
	Copyright (c) 2007, 2008 Jacob Beller, Hanno Braun, Jan Lücker
	
	Permission to use, copy, modify, and/or distribute this software for any
	purpose with or without fee is hereby granted, provided that the above
	copyright notice and this permission notice appear in all copies.

	THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
	WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
	MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
	ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
	WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
	ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
	OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*/



package de.elrador.treibjagd.common.game.cards.dwarves;



import de.elrador.treibjagd.common.game.PlayArea;
import de.elrador.treibjagd.common.game.Field;
import de.elrador.treibjagd.common.game.cards.EffectCard;
import de.elrador.treibjagd.common.game.effects.EffectPulverfass;



/**
 * 1 Esel erhält 3 Runden lang ein Pulverfass, stirbt er, so sterben auch die anderen Tiere in der Linie 
 * (dafür erhält man keine Punkte)
 */

public class Pulverfass extends EffectCard {

	public byte id() {
		return 52;
	}

	public CardType type() {
		return CardType.ZWERG;
	}
	
	public int number() {
		return 1;
	}
	
	public String graphicsPath() {
		return "zwerg/pulverfass.png";
	}
	
	public void play(PlayArea area, Target... targets) {
		Field field = area.field(targets[0].x, targets[0].y);
		field.card().effectHandler().addEffect(
			new EffectPulverfass(area, field.card(), area.currentTurnPlayer(),
			  3*area.players().length));
	}
	
	public boolean isTargetValid(PlayArea area, Target... targets) {
		Field field = area.field(targets[0].x, targets[0].y);
		if(field.card() == null) return false;
		return field.card().player() == area.currentTurnPlayer();
	}
	
	public byte getNumberOfTargets() {
		return 1;
	}
}
