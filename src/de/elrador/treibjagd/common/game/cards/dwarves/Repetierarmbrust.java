/*
	Copyright (c) 2007, 2008 Jacob Beller, Hanno Braun, Jan Lücker
	
	Permission to use, copy, modify, and/or distribute this software for any
	purpose with or without fee is hereby granted, provided that the above
	copyright notice and this permission notice appear in all copies.

	THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
	WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
	MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
	ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
	WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
	ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
	OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*/



package de.elrador.treibjagd.common.game.cards.dwarves;



import de.elrador.treibjagd.common.game.PlayArea;
import de.elrador.treibjagd.common.game.cards.EffectCard;



/**
 * Schuss auf 2 benachbarte, beleuchtete Felder (in Reihe oder Linie)
 */

public class Repetierarmbrust extends EffectCard {

	public byte id() {
		return 53;
	}

	public CardType type() {
		return CardType.ZWERG;
	}
	
	public int number() {
		return 2;
	}
	
	public String graphicsPath() {
		return "zwerg/repetierarmbrust.png";
	}
	
	public void play(PlayArea area, Target... targets) {
		if(targets[1].x > targets[0].x) {
			/* Work around for uninitialized field cards */
			Target tmp = targets[0];
			targets[0] = targets[1];
			targets[1] = tmp;
		}
		for(int i=0; i<2; i++) {
			area.shootCard(targets[i].x, targets[i].y, area.currentTurnPlayer());
			area.field(targets[i].x, targets[i].y).illuminated(false);
		}
	}
	
	public boolean isTargetValid(PlayArea area, Target... targets) {
		if(targets.length == 1)
			return area.field(targets[0].x, targets[0].y).illuminated();
		else {
			int dx = targets[0].x - targets[1].x;
			int dy = targets[0].y - targets[1].y;
			if(Math.pow(dx,2) + Math.pow(dy,2) != 1) return false;
			return area.field(targets[1].x, targets[1].y).illuminated();
		}
	}
	
	public byte getNumberOfTargets() {
		return 2;
	}
}
