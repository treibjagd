/*
	Copyright (c) 2007, 2008 Jacob Beller, Hanno Braun, Jan Lücker
	
	Permission to use, copy, modify, and/or distribute this software for any
	purpose with or without fee is hereby granted, provided that the above
	copyright notice and this permission notice appear in all copies.

	THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
	WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
	MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
	ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
	WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
	ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
	OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*/



package de.elrador.treibjagd.common.game.cards.basic;



import de.elrador.treibjagd.common.game.PlayArea;
import de.elrador.treibjagd.common.game.cards.EffectCard;



/**
 * eine Reihe 1 vor
 */

public class Marschieren extends EffectCard {

	public byte id() {
		return 15;
	}

	public CardType type() {
		return CardType.BASIC;
	}
	
	public int number() {
		return 2;
	}
	
	public String graphicsPath() {
		return "common/marschieren.png";
	}
	
	public void play(PlayArea area, Target... targets) {
		area.moveCard((byte)0, targets[0].y, (byte)1);
	}
	
	public boolean isTargetValid(PlayArea area, Target... targets) {
		return true;
	}
	
	public byte getNumberOfTargets() {
		return 1;
	}
}
