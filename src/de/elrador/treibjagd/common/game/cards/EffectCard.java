/*
	Copyright (c) 2008 Hanno Braun <hanno@elrador.de>
	All rights reserved.
*/



package de.elrador.treibjagd.common.game.cards;



import de.elrador.treibjagd.common.game.PlayArea;


/**
 * Base class for all effect cards.
 */

public abstract class EffectCard {
	
	/**
	 * There are several types of cards: Basic cards that can be used by all races and cards that can only be
	 * used by one race.
	 */
	
	public enum CardType {BASIC, ELF, ORK, MENSCH, ZWERG}
	
	
	
	/**
	 * Target coordinates for an effect card.
	 */
	
	public static class Target {
		public final byte x;
		public final byte y;
		
		public Target(byte xPos, byte yPos) {
			x = xPos;
			y = yPos;
		}
	}
	
	
	
	/**
	 * Returns the id of the card. Each effect card must have a unique id number. This number will be used to
	 * identify the card when sent over a network stream.
	 */
	
	public abstract byte id();
	
	
	
	/**
	 * Returns the type of the card, as defined by EffectCard.CardType.
	 */

	public abstract CardType type();
	
	
	
	/**
	 * Returns the number of cards of this type that are in a player's deck at the beginning of the game.
	 */
	
	public abstract int number();
	
	
	
	/**
	 * Returns the path of the graphic that represents this card.
	 */
	
	public abstract String graphicsPath();
	
	
	
	/**
	 * Returns the number of targets this card needs.
	 */
	
	public abstract byte getNumberOfTargets();
	
	
	
	/**
	 * Returns true if the given sequence of targets is valid, false if it is not.
	 */
	
	public abstract boolean isTargetValid(PlayArea area, Target... targets);
	
	
	
	/**
	 * Plays the card on the given target coordinates of the given play area.
	 */
	
	public abstract void play(PlayArea area, Target... targets);
}
