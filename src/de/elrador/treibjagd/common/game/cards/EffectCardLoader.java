/*
	Copyright (c) 2007, 2008 Jacob Beller, Hanno Braun, Jan Lücker
	
	Permission to use, copy, modify, and/or distribute this software for any
	purpose with or without fee is hereby granted, provided that the above
	copyright notice and this permission notice appear in all copies.

	THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
	WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
	MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
	ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
	WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
	ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
	OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*/



package de.elrador.treibjagd.common.game.cards;


import de.elrador.treibjagd.common.game.cards.basic.Ausscheren;
import de.elrador.treibjagd.common.game.cards.basic.Beleuchten;
import de.elrador.treibjagd.common.game.cards.basic.Daneben;
import de.elrador.treibjagd.common.game.cards.basic.Irrlicht;
import de.elrador.treibjagd.common.game.cards.basic.Lichterkette;
import de.elrador.treibjagd.common.game.cards.basic.Marschieren;
import de.elrador.treibjagd.common.game.cards.basic.Schießen;
import de.elrador.treibjagd.common.game.cards.basic.Verstecken;
import de.elrador.treibjagd.common.game.cards.basic.Vordrängeln;
import de.elrador.treibjagd.common.game.cards.dwarves.Ölbombe;
import de.elrador.treibjagd.common.game.cards.dwarves.Öllaterne;
import de.elrador.treibjagd.common.game.cards.dwarves.Pulverfass;
import de.elrador.treibjagd.common.game.cards.dwarves.Repetierarmbrust;
import de.elrador.treibjagd.common.game.cards.dwarves.Steinschlag;
import de.elrador.treibjagd.common.game.cards.dwarves.StörrischerEsel;
import de.elrador.treibjagd.common.game.cards.elves.Austreten;
import de.elrador.treibjagd.common.game.cards.elves.Dornenranken;
import de.elrador.treibjagd.common.game.cards.elves.Falle;
import de.elrador.treibjagd.common.game.cards.elves.Lichtelfe;
import de.elrador.treibjagd.common.game.cards.elves.Nebel;
import de.elrador.treibjagd.common.game.cards.elves.Stampede;
import de.elrador.treibjagd.common.game.cards.humans.Diebstahl;
import de.elrador.treibjagd.common.game.cards.humans.Massenpanik;
import de.elrador.treibjagd.common.game.cards.humans.Meisterjäger;
import de.elrador.treibjagd.common.game.cards.humans.Notschlachtung;
import de.elrador.treibjagd.common.game.cards.humans.RücksichtsloseHast;
import de.elrador.treibjagd.common.game.cards.humans.Waffenstillstand;
import de.elrador.treibjagd.common.game.cards.orcs.DenLetztenBeißenDieWölfe;
import de.elrador.treibjagd.common.game.cards.orcs.Dunkelheit;
import de.elrador.treibjagd.common.game.cards.orcs.Geisterwolf;
import de.elrador.treibjagd.common.game.cards.orcs.SchnappsDir;
import de.elrador.treibjagd.common.game.cards.orcs.Tollwut;
import de.elrador.treibjagd.common.game.cards.orcs.Wolfsgeheul;
import de.elrador.treibjagd.common.game.Race;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;



/**
 * Instantiates all effect cards
 */

public class EffectCardLoader {
	
	private static final EffectCard[] effectCards = new EffectCard[] {
			new Ausscheren(), new Beleuchten(), new Daneben(), new Irrlicht(), new Lichterkette(),
			new Marschieren(), new Schießen(), new Verstecken(), new Vordrängeln(), new Ölbombe(),
			new Öllaterne(), new Pulverfass(), new Repetierarmbrust(), new Steinschlag(),
			new StörrischerEsel(), new Austreten(), new Dornenranken(), new Falle(), new Lichtelfe(),
			new Nebel(), new Stampede(), new Diebstahl(), new Massenpanik(), new Meisterjäger(),
			new Notschlachtung(), new RücksichtsloseHast(), new Waffenstillstand(),
			new DenLetztenBeißenDieWölfe(), new Dunkelheit(), new Geisterwolf(), new SchnappsDir(),
			new Tollwut(), new Wolfsgeheul()};
	
	
	
	private Map<Race, List<EffectCard>> cards;
	private Map<Byte, EffectCard> cardTypesById;
	
	
	
	/**
	 * Loads all effect cards and saves them into the different sets.
	 */
	
	public EffectCardLoader() {
		cards = new HashMap<Race, List<EffectCard>>();
		cards.put(Race.ZWERGE, new LinkedList<EffectCard>());
		cards.put(Race.ELFEN, new LinkedList<EffectCard>());
		cards.put(Race.MENSCHEN, new LinkedList<EffectCard>());
		cards.put(Race.ORKS, new LinkedList<EffectCard>());
		
		cardTypesById = new HashMap<Byte, EffectCard>();
		
		// Instantiate all cards and sort them into the different sets.
		for (EffectCard card : effectCards) {
			cardTypesById.put(card.id(), card);
			for (int i = 0; i < card.number(); i++) {
				if (card.type() == EffectCard.CardType.BASIC) {
					cards.get(Race.ZWERGE).add(card);
					cards.get(Race.ELFEN).add(card);
					cards.get(Race.MENSCHEN).add(card);
					cards.get(Race.ORKS).add(card);
				}
				else if (card.type() == EffectCard.CardType.ZWERG) {
					cards.get(Race.ZWERGE).add(card);
				}
				else if (card.type() == EffectCard.CardType.ELF) {
					cards.get(Race.ELFEN).add(card);
				}
				else if (card.type() == EffectCard.CardType.MENSCH) {
					cards.get(Race.MENSCHEN).add(card);
				}
				else if (card.type() == EffectCard.CardType.ORK) {
					cards.get(Race.ORKS).add(card);
				}
			}
		}
	}
	
	
	
	/**
	 * Returns the effect card set for the given race. 
	 */
	
	public List<EffectCard> getCards(Race race) {
		return new LinkedList<EffectCard>(cards.get(race));
	}
	
	
	
	/**
	 * Returns the card type class with the given id.
	 */
	
	public EffectCard cardById(byte id) {
		if (!cardTypesById.containsKey(id)) {
			throw new IllegalArgumentException("Unknown id: " + id);
		}
		
		return cardTypesById.get(id);
	}
}
