/*
	Copyright (c) 2007, 2008 Jacob Beller, Hanno Braun, Jan Lücker
	
	Permission to use, copy, modify, and/or distribute this software for any
	purpose with or without fee is hereby granted, provided that the above
	copyright notice and this permission notice appear in all copies.

	THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
	WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
	MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
	ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
	WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
	ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
	OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*/



package de.elrador.treibjagd.common.game.cards.orcs;



import de.elrador.treibjagd.common.game.PlayArea;
import de.elrador.treibjagd.common.game.cards.EffectCard;



/**
 * eine Reihe 1 Feld zurück (vorne erscheint 1 leeres Feld, letztes Feld kommt auf den Nachschubstapel)
 */

public class Wolfsgeheul extends EffectCard {

	public byte id() {
		return 45;
	}

	public CardType type() {
		return CardType.ORK;
	}
	
	public int number() {
		return 2;
	}
	
	public String graphicsPath() {
		return "ork/wolfsgeheul.png";
	}
	
	public void play(PlayArea area, Target... targets) {
		area.putCardBackOnReinforcemenStack((byte)4, targets[0].y);
	}
	
	public boolean isTargetValid(PlayArea area, Target... targets) {
		return targets.length == 1;
	}
	
	public byte getNumberOfTargets() {
		return 1;
	}
}
