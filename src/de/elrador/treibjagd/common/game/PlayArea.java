/*
	Copyright (c) 2007, 2008 Jacob Beller, Hanno Braun, Jan Lücker
	
	Permission to use, copy, modify, and/or distribute this software for any
	purpose with or without fee is hereby granted, provided that the above
	copyright notice and this permission notice appear in all copies.

	THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
	WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
	MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
	ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
	WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
	ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
	OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*/



package de.elrador.treibjagd.common.game;

import de.elrador.treibjagd.common.game.effects.EffectVerstecken;

import java.util.LinkedList;
import java.util.List;
import java.util.Random;



/**
 * Models the main play area. Holds the relevant game data and provides logic to manipulate this data.
 * This class is shared between client and server. It is generated on the server and each client, using a
 * shared random seed, that is provided by the server.
 */

public class PlayArea implements GameConstants {
	
	private enum State {CREATED, INITIALIZED};
	
	
	
	private Player[] players;
	private State state;
	private Field[][] fieldCards;
	
	private Random r;
	
	private byte xInit;
	private byte yInit;
	
	private Player currentTurnPlayer;
	
	private List<Byte> nextCardFromStackGoesHereQueue;
	
	private List<FieldCard> locallyKnownTopOfReinforcementStack;
	private NewCardNeededListener listener;
	
	private int cardsOnReinforcementStack;
	
	
	
	/**
	 * Creates the play area, initializing it with an initial game state, as defined by the rules. All random
	 * numbers needed to initialize the play area are derived from a provided seed, to guarantee indentical
	 * game state on all clients.
	 */
	
	public PlayArea(Player[] thePlayers, long seed, NewCardNeededListener aListener) {
		players = thePlayers;
		state = State.CREATED;
		fieldCards = new Field[5][3];
		listener = aListener;
		
		r = new Random(seed);
		
		xInit = 2;
		yInit = 0;
		
		for (byte x = 0; x < fieldCards.length; x++) {
			for (byte y = 0; y < fieldCards[0].length; y++) {
				fieldCards[x][y] = new Field();
				fieldCards[x][y].card(new FieldCard(FieldCard.Type.EMPTY));
			}
		}
		
		nextCardFromStackGoesHereQueue = new LinkedList<Byte>();
		locallyKnownTopOfReinforcementStack = new LinkedList<FieldCard>();
		
		cardsOnReinforcementStack = STARTINGSTACK_ANIMALS_PER_PLAYER * players.length
				+ REINFORCEMENTSTACK_ANIMALS_PER_PLAYER * players.length
				+ REINFORCEMENTSTACK_EMPTY_PER_PLAYER * players.length
				+ REINFORCEMENTSTACK_ADDED_EMPTY;
	}
	
	
	
	/**
	 * Calls the other constructor with a listener that does nothing.
	 */
	
	public PlayArea(Player[] thePlayers, long seed) {
		this(thePlayers, seed, new NewCardNeededListener() {
			public void newCardNeeded() {
				// Useful things could happen here. Let's just do nothing though.
			}
		});
	}
	
	
	
	/**
	 * Notifies the play area that a new card was drawn from the reinforcement stack. The play area then puts
	 * The card where it belongs, depending on the current state of the game.
	 * 
	 * @param card A field card with an associated player.
	 */
	
	public void drawnCardFromStack(FieldCard card) {
		if (state == State.CREATED) {
			fieldCards[xInit][yInit].card(card);
			
			xInit++;
			if (xInit == fieldCards.length) {
				xInit = 2;
				yInit++;
				
				// Check if all the initial cards have been placed.
				if (yInit == fieldCards[0].length) {
					// Look at all the fields to check the position of all players first animals.
					byte[] firstAnimalPositions = new byte[players.length];
					boolean[] firstAnimalFound = new boolean[players.length];
					for (byte i = 0; i < fieldCards.length; i++) {
						for (byte j = 0; j < fieldCards[i].length; j++) {
							Player player = fieldCards[i][j].card().player();
							if (player != null) {
								if (!firstAnimalFound[player.id()]) {
									firstAnimalPositions[player.id()] = i;
									firstAnimalFound[player.id()] = true;
								}
							}
						}
					}
					
					// Check which position is the nearest to the right side.
					int max = 0;
					for (int position : firstAnimalPositions) {
						if (position > max) {
							max = position;
						}
					}
					
					// Generate a list of players that are candidates for the first turn.
					List<Player> firstTurnCandidates = new LinkedList<Player>();
					for (byte i = 0; i < players.length; i++) {
						if (firstAnimalPositions[i] == max) {
							firstTurnCandidates.add(players[i]);
						}
					}
					
					// Choose one player from the list of candidates.
					currentTurnPlayer = firstTurnCandidates.get(r.nextInt(firstTurnCandidates.size()));
					
					// Play area has been initialized, set state.
					state = State.INITIALIZED;
				}
			}
		}
		else {
			if (nextCardFromStackGoesHereQueue.isEmpty()) {
				locallyKnownTopOfReinforcementStack.add(locallyKnownTopOfReinforcementStack.size(),	card);
			}
			else {
				byte x = (byte)(fieldCards.length - 1);
				byte y = nextCardFromStackGoesHereQueue.remove(0);
			
				while (x != 0 && fieldCards[x - 1][y].card() == null) {
					x--;
				}
			
				fieldCards[x][y].card(card);
			}
		}
	}
	
	
	
	/**
	 * Notifies the play area that a new card was drawn from the reinforcement stack. The play area then puts
	 * The card where it belongs, depending on the current state of the game.
	 * 
	 * @param card A field card without an associated player.
	 * @param playerId The id of the player that should be associated to the field card.
	 * @param illuminated The illumination status of the card.
	 */
	
	public void drawnCardFromStack(FieldCard card, byte playerId) {
		if (card.type() != FieldCard.Type.EMPTY) {
			card.player(players[playerId]);
		}
		drawnCardFromStack(card);
	}
	
	
	
	/**
	 * Returns the card on the specified field.
	 */
	
	public Field field(int x, int y) {
		if (x < 0 || x > fieldCards.length || y < 0 || y > fieldCards[0].length) {
			throw new IllegalArgumentException("Coordinates out of bounds (x: " + x + ", y: " + y + ").");
		}
		
		return fieldCards[x][y];
	}
	
	
	
	/**
	 * Returns the players.
	 */
	
	public Player[] players() {
		return players;
	}
	
	
	
	/**
	 * Returns the player whose turn it is.
	 */
	
	public Player currentTurnPlayer() {
		return currentTurnPlayer;
	}
	
	
	
	/**
	 * It's the next player's turn.
	 */
	
	public void turnChange() {
		if (currentTurnPlayer.id() + 1 >= players.length) {
			currentTurnPlayer = players[0];
		}
		else {
			currentTurnPlayer = players[currentTurnPlayer.id() + 1];
		}
		
		runEffects(1);
	}
	
	private void runEffects(int turns) {
		for(byte x=0; x<5; x++) {
			for(byte y=0; y<3; y++) {
				fieldCards[x][y].effectHandler().runEffects(turns);
				if(fieldCards[x][y].card() != null)
					fieldCards[x][y].card().effectHandler().runEffects(turns);
			}
		}
	}
	
	/**
	 * Removes an animal.
	 */
	
	public void removeCard(byte x, byte y) {
		for (byte i = x; i < fieldCards.length - 1; i++) {
			fieldCards[i][y].card(fieldCards[i + 1][y].card());
		}
		fieldCards[fieldCards.length - 1][y].card(null);
		
		if (locallyKnownTopOfReinforcementStack.isEmpty()) {
			nextCardFromStackGoesHereQueue.add(y);
			listener.newCardNeeded();
			cardsOnReinforcementStack -= 1;
		}
		else {
			fieldCards[fieldCards.length - 1][y].card(locallyKnownTopOfReinforcementStack.remove(0));
		}
	}
	
	
	
	/**
	 * Removes the card on the given position from the field and puts it on the local reinforcement stack.
	 * Moves all animals in front of that card one field backwards and puts an empty field card on the first
	 * field.
	 */
	
	public void putCardBackOnReinforcemenStack(byte x, byte y) {
		locallyKnownTopOfReinforcementStack.add(0, fieldCards[x][y].card());
		cardsOnReinforcementStack += 1;
		
		for (byte i = x; i >= 1; i--) {
			fieldCards[i][y].card(fieldCards[i - 1][y].card());
		}
		fieldCards[0][y].card(new FieldCard(FieldCard.Type.EMPTY));
	}
	
	
	
	/**
	 * Moves an animal forward or backward.
	 * 
	 * @param distance Positive: Forward, negative: Backwards.
	 */
	
	public void moveCard(byte x, byte y, byte distance) {
		if(x - distance < 0) {
			if(field(x,y).card().type() != FieldCard.Type.EMPTY) {
				field(x,y).card().player().increaseScore(3);
				field(x, y).card().player().decreaseAnimals(1);
			}
			removeCard(x, y);
			return;
		}
		if(x - distance > 5) {
			// TODO!
		}
		byte direction = ((byte)(distance / Math.abs(distance))); // +1 for forward, -1 for backward
		FieldCard tmp = fieldCards[x][y].card();
		for (int i = 0; i < distance; i++) {
			fieldCards[x - (i * direction)][y].card(fieldCards[x - ((i + 1) * direction)][y].card());
			//TODO: runEffects(0);
		}
		fieldCards[x - distance][y].card(tmp);
	}
	
	
	
	/**
	 * Moves an animal sideways.
	 * 
	 * @param distance Positive: Down, negative: Up.
	 */
	
	public void moveCardSideWays(byte x, byte y, byte distance) {
		byte direction = ((byte)(distance / Math.abs(distance))); // +1 for down, -1 for up
		FieldCard tmp = fieldCards[x][y].card();
		for (int i = 0; i < Math.abs(distance); i++) {
			fieldCards[x][y + (i * direction)].card(fieldCards[x][y + ((i + 1) * direction)].card());
			// TODO: runEffects(0);
		}
		fieldCards[x][y + distance].card(tmp);
	}
	
	
	
	/**
	 * Shoots at the card at the given coordinates.
	 */
	
	public void shootCard(byte x, byte y, Player shooter) {
		if(field(x, y).card().effectHandler().effectPresent(EffectVerstecken.class)) return;
		if(field(x, y).card().type() != FieldCard.Type.EMPTY) {
			field(x, y).card().player().decreaseAnimals(1);
			if(field(x, y).card().player() != shooter) {
				shooter.increaseScore(1);
			} else {
				shooter.increaseScore(0);
			}
			removeCard(x, y);
		}
	}
	
	
	
	/**
	 * Return a number in [0,max-1] by using the seed from the server
	 */
	
	public byte randomByte(byte max) {
		return (byte)r.nextInt(max);
	}
	
	
	
	/**
	 * Saves the position of card in pos
	 */
	
	public void findCard(FieldCard card, byte[] pos) {
		for(byte x=0; x<5; x++) {
			for(byte y=0; y<3; y++) {
				if(fieldCards[x][y].card() == card) {
					pos[0] = x;
					pos[1] = y;
					return;
				}
			}
		}
	}
	
	
	
	/**
	 * Returns the number of cards left on the reinforcement stack.
	 */
	
	public int cardsOnReinforcementStack() {
		return cardsOnReinforcementStack;
	}
}

