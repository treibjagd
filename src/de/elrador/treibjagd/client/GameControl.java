/*
	Copyright (c) 2007, 2008 Jacob Beller, Hanno Braun, Jan Lücker
	
	Permission to use, copy, modify, and/or distribute this software for any
	purpose with or without fee is hereby granted, provided that the above
	copyright notice and this permission notice appear in all copies.

	THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
	WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
	MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
	ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
	WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
	ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
	OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*/



package de.elrador.treibjagd.client;



import de.elrador.treibjagd.client.controller.main.EffectCardPlayController;
import de.elrador.treibjagd.client.game.*;
import de.elrador.treibjagd.client.view.end.EndFrame;
import de.elrador.treibjagd.client.view.game.GameListener;
import de.elrador.treibjagd.client.view.game.GameView;
import de.elrador.treibjagd.client.view.login.LoginListener;
import de.elrador.treibjagd.client.view.login.LoginView;
import de.elrador.treibjagd.client.view.main.MainView;
import de.elrador.treibjagd.common.game.PlayArea;
import de.elrador.treibjagd.common.game.Player;
import de.elrador.treibjagd.common.game.cards.EffectCard;
import de.elrador.treibjagd.common.game.cards.EffectCardLoader;
import de.elrador.treibjagd.common.network.LastMessageInvalidMessage;
import de.elrador.treibjagd.common.network.gamemanagement.JoinedGameMessage;
import de.elrador.treibjagd.common.network.gamemanagement.JoinGameMessage;
import de.elrador.treibjagd.common.network.ingame.CardFromReinforcementStackMessage;
import de.elrador.treibjagd.common.network.ingame.ChangeCardInHandMessage;
import de.elrador.treibjagd.common.network.ingame.EffectCardPlayedMessage;
import de.elrador.treibjagd.common.network.ingame.GameEndsMessage;
import de.elrador.treibjagd.common.network.ingame.GameStartedMessage;
import de.elrador.treibjagd.common.network.ingame.StartGameMessage;
import de.elrador.treibjagd.common.network.ingame.TurnChangeMessage;
import de.elrador.treibjagd.common.network.init.NameAcceptedMessage;
import de.elrador.treibjagd.common.network.init.SetNameMessage;

import java.io.IOException;
import java.net.InetSocketAddress;

import ioproject.client.network.Server;
import ioproject.client.network.ServerHandler;
import ioproject.common.network.Node;



/**
 * Controls the game.
 */

public class GameControl implements ServerHandler, LoginListener, GameListener {
	
	private enum State {STARTED, LOGGED_IN, IN_LOBBY, IN_GAME, GAME_STARTED}
	
	private Object synchronizer;
	
	private State state;
	private Server server;
	
	private LoginView loginView;
	private GameView gameView;
	private MainView mainView;
	
	private byte race;
	
	private EffectCardLoader loader;
	
	private PlayArea playArea;
	private LocalPlayer localPlayer;
	
	private LastPlayedCard lastPlayedCard;
	private Player playerWhoPlayedLastCard;
	
	
	
	/**
	 * Constructor.
	 */
	
	public GameControl() {
		synchronizer = new Object();
		
		state = State.STARTED;
		
		server = new Server();
		
		loader = new EffectCardLoader();
		
		lastPlayedCard = new LastPlayedCard();
		playerWhoPlayedLastCard = null;
		
		loginView = new LoginView();
       	loginView.addLoginListener(this);
       	loginView.setVisible(true);
		
		gameView = new GameView();
		gameView.addGameListener(this);
	}
	
	
	
	/**
	 * Handles network erros.
	 */
	
	public void exceptionCaught(Server server, Throwable cause) {
		// TODO: a message is probably not enough....
		/*JOptionPane.showMessageDialog(null, cause.getLocalizedMessage(),
			"Treibjagd", JOptionPane.ERROR_MESSAGE);*/
		System.out.println("Exception in thread \"" + Thread.currentThread().getName() + "\":");
		cause.printStackTrace();
		System.exit(1);
	}


	
	/**
	 * Handles incoming messages.
	 */

	public void messageReceived(Node node, Object message) {
		synchronized (synchronizer) {
			if (message instanceof LastMessageInvalidMessage) {
				System.out.println("Invalid message - message received!");
				System.exit(1);
			}
			
			if (state == State.STARTED) {
				throw new AssertionError("Invalid message: " + message + " (State: " + state + ")");
			}
			else if (state == State.LOGGED_IN) {
				if (message instanceof NameAcceptedMessage) {
					if (((NameAcceptedMessage)message).accepted) {
						state = State.IN_LOBBY;
						server.send(new JoinGameMessage("", race));
					}
					else {
						loginView.setVisible(true);
					}
				}
				else {
					throw new AssertionError("Invalid message: " + message + " (State: " + state + ")");
				}
			}
			else if (state == State.IN_LOBBY) {
				if (message instanceof JoinedGameMessage) {
					if (((JoinedGameMessage)message).joined) {
						gameView.setVisible(true);
						state = State.IN_GAME;
					}
					else {
						System.out.println("Game full.");
						System.exit(0);
					}
				}
				else {
					throw new AssertionError("Invalid message: " + message + " (State: " + state + ")");
				}
			}
			else if (state == State.IN_GAME) {
				if (message instanceof GameStartedMessage) {
					GameStartedMessage gameStartedMessage = (GameStartedMessage)message;
					Player[] players = gameStartedMessage.players();
					
					// Game logic changes
					playArea = new PlayArea(players, gameStartedMessage.seed);
					localPlayer = new LocalPlayer(players[gameStartedMessage.id]);
					
					// View changes
					gameView.setVisible(false);
					FieldSelection fieldSelection = new FieldSelection(playArea);
					mainView = new MainView(playArea, localPlayer, fieldSelection, lastPlayedCard);
					EffectCardPlayController effectController = new EffectCardPlayController(
							synchronizer, server, playArea, localPlayer, mainView, fieldSelection);
					mainView.addListeners(effectController.handListener(), effectController.fieldListener());
					mainView.setVisible();				
					
					// State change
					state = State.GAME_STARTED;
				}
				else {
					throw new AssertionError("Invalid message: " + message + " (State: " + state + ")");
				}
			}
			else if (state == State.GAME_STARTED) {
				if (message instanceof TurnChangeMessage) {
					if (playerWhoPlayedLastCard != null
							&& playerWhoPlayedLastCard != playArea.currentTurnPlayer()) {
						lastPlayedCard.setCard(null);
					}
					playArea.turnChange();
					mainView.update();
				}
				else if (message instanceof ChangeCardInHandMessage) {
					ChangeCardInHandMessage changeMessage = (ChangeCardInHandMessage)message;
					EffectCard card = loader.cardById(changeMessage.cardId);
					localPlayer.hand().setCard(changeMessage.slotId, card);
					localPlayer.decreaseEffectCards();
					mainView.update();
				}
				else if (message instanceof CardFromReinforcementStackMessage) {
					CardFromReinforcementStackMessage m = (CardFromReinforcementStackMessage)message;
					playArea.drawnCardFromStack(m.card(), m.playerId);
					mainView.update();
				}
				else if (message instanceof GameEndsMessage) {
					new EndFrame(playArea.players(), localPlayer.player()).setVisible(true);
				}
				else if (message instanceof EffectCardPlayedMessage) {
					EffectCardPlayedMessage cardPlayedMessage = (EffectCardPlayedMessage)message;
					EffectCard card = loader.cardById(cardPlayedMessage.cardId);
					card.play(playArea, cardPlayedMessage.targets());
					lastPlayedCard.setCard(card);
					playerWhoPlayedLastCard = playArea.currentTurnPlayer();
				}
				else {
					throw new AssertionError("Invalid message: " + message + " (State: " + state + ")");
				}
			}
		}
	}
		
	
	
	/**
	 * Handles outgoing message.
	 */

	public void messageSent(Node server, Object message) {
		// Not interested in outgoing message.
	}
	
	
	
	/**
	 * Implements Server.connectionClosed().
	 * Called by the framework when the connection to the server has been closed.
	 */
	
	public void connectionClosed(Server server) {
		// This event is currently ignored.
	}
	
	
	
	/**
	 * Reacts to the user's login request.
	 */
	
	public void login(String name, byte aRace, String host) throws IOException {
		synchronized (synchronizer) {
			race = aRace;
			
			loginView.setVisible(false);
			
			if (state == State.STARTED) {
				server.connect(new InetSocketAddress(host, 34480), this);
				state = State.LOGGED_IN;
				server.send(new SetNameMessage(name));
			}
			else  if (state == State.LOGGED_IN) {
				server.send(new SetNameMessage(name));
			}
			else {
				throw new AssertionError("Invalid state: " + state);
			}
		}
	}
	
	
	
	/**
	 * Reacts when the user wants to start the game.
	 */
	
	public void startRequested() {
		synchronized (synchronizer) {
			if (state == State.IN_GAME) {
				gameView.disable();
				server.send(new StartGameMessage());
			}
			else {
				throw new AssertionError("Invalid state: " + state);
			}
		}
	}
}
