/*
	Copyright (c) 2007, 2008 Jacob Beller, Hanno Braun, Jan Lücker
	
	Permission to use, copy, modify, and/or distribute this software for any
	purpose with or without fee is hereby granted, provided that the above
	copyright notice and this permission notice appear in all copies.

	THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
	WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
	MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
	ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
	WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
	ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
	OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*/



package de.elrador.treibjagd.client.controller.main;



import de.elrador.treibjagd.client.game.LocalPlayer;
import de.elrador.treibjagd.common.game.PlayArea;
import de.elrador.treibjagd.common.game.cards.EffectCard;
import de.elrador.treibjagd.common.network.ingame.DropCardMessage;
import de.elrador.treibjagd.common.network.ingame.PlayEffectCardMessage;

import ioproject.client.network.Server;



/**
 * When the player wants to play or drop a card, this class sends the info to the server.
 */

class EffectCardNetworkSender {
	
	private Server server;
	private PlayArea playArea;
	private LocalPlayer localPlayer;
	
	
	
	/**
	 * Saves references blabla.
	 */
	
	EffectCardNetworkSender(Server theServer, PlayArea thePlayArea,	LocalPlayer theLocalPlayer) {
		server = theServer;
		playArea = thePlayArea;
		localPlayer = theLocalPlayer;
	}
	
	
	
	/**
	 * Checks if it's the local player's turn. If yes, sends a message to the server that the player wants to
	 * play the given card.
	 * 
	 * @return True, if the message is being sent, false otherwise.
	 */
	
	boolean playCard(byte cardIndex, EffectCard.Target... targets) {
		if (playArea.currentTurnPlayer() != localPlayer.player()) {
			return false;
		}
		
		server.send(new PlayEffectCardMessage(cardIndex, targets));
		return true;
	}
	
	
	
	/**
	 * Checks if it's the local player's turn. If yes, sends a message to the server the the player wants to
	 * drop the given card.
	 * 
	 * @return True, if the message is being sent, false otherwise.
	 */
	
	boolean dropCard(byte cardIndex) {
		if (playArea.currentTurnPlayer() != localPlayer.player()) {
			return false;
		}
		
		server.send(new DropCardMessage(cardIndex));
		return true;
	}
}
