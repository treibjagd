/*
	Copyright (c) 2007, 2008 Jacob Beller, Hanno Braun, Jan Lücker
	
	Permission to use, copy, modify, and/or distribute this software for any
	purpose with or without fee is hereby granted, provided that the above
	copyright notice and this permission notice appear in all copies.

	THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
	WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
	MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
	ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
	WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
	ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
	OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*/



package de.elrador.treibjagd.client.controller.main;



import de.elrador.treibjagd.client.game.LocalPlayer;
import de.elrador.treibjagd.client.game.FieldSelection;
import de.elrador.treibjagd.client.view.main.HandLabel;
import de.elrador.treibjagd.client.view.main.MainView;
import de.elrador.treibjagd.common.game.PlayArea;

import java.awt.event.MouseEvent;
import java.awt.event.MouseAdapter;



/**
 * Listens for mouse events from the hand.
 */
public class HandListener extends MouseAdapter {
	
	private Object synchronizer;
	
	private PlayArea playArea;
	private LocalPlayer localPlayer;
	
	private FieldSelection selectionManager;
	private EffectCardNetworkSender sender;
	
	private MainView view;
	
	
	
	/**
	 * Saves the reference to the selection manager.
	 */
	
	HandListener(Object theSynchronizer, PlayArea thePlayArea, LocalPlayer theLocalPlayer,
			FieldSelection theSelectionManager, EffectCardNetworkSender theSender, MainView theView) {
		synchronizer = theSynchronizer;
		
		playArea = thePlayArea;
		localPlayer = theLocalPlayer;
		
		selectionManager = theSelectionManager;
		sender = theSender;
		
		view = theView;
	}
	
	
	
	/**
	 * Implements MouseListener.mouseClicked().
	 */
	
	@Override
	public void mouseClicked(MouseEvent e) {
		synchronized (synchronizer) {
			if (playArea.currentTurnPlayer() != localPlayer.player()) {
				return;
			}
			
			HandLabel source = (HandLabel)e.getSource();
			byte index = source.index();
			
			if (e.getButton() == MouseEvent.BUTTON1) {
				if (localPlayer.hand().selectedCardIndex() != index) {
					localPlayer.hand().selectCard(index);
				}
				else {
					localPlayer.hand().deselectCard();
				}
			}
			else if (e.getButton() == MouseEvent.BUTTON3) {
				if (sender.dropCard(index)) {
					selectionManager.clearSelection();
				}
			}
			
			view.update();
		}
	}
	
	
	
	/**
	 * Implements MouseListener.mouseEntered().
	 */
	
	@Override
	public void mouseEntered(MouseEvent e) {
		((HandLabel)e.getSource()).addTemporaryHighlight();
	}
	
	
	
	/**
	 * Implements MouseListener.mouseExited().
	 */
	
	@Override
	public void mouseExited(MouseEvent e) {
		((HandLabel)e.getSource()).removeTemporaryHighlight();
	}
}
