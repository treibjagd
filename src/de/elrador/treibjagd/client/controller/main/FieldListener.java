/*
	Copyright (c) 2007, 2008 Jacob Beller, Hanno Braun, Jan Lücker
	
	Permission to use, copy, modify, and/or distribute this software for any
	purpose with or without fee is hereby granted, provided that the above
	copyright notice and this permission notice appear in all copies.

	THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
	WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
	MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
	ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
	WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
	ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
	OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*/



package de.elrador.treibjagd.client.controller.main;



import de.elrador.treibjagd.client.game.LocalPlayer;
import de.elrador.treibjagd.client.game.FieldSelection;
import de.elrador.treibjagd.client.view.main.FieldLabel;
import de.elrador.treibjagd.client.view.main.MainView;
import de.elrador.treibjagd.common.game.PlayArea;
import de.elrador.treibjagd.common.game.cards.EffectCard;

import java.awt.event.MouseEvent;
import java.awt.event.MouseAdapter;



/**
 * Listens to mouse events from the field.
 */

public class FieldListener extends MouseAdapter {
	
	private Object synchronizer;
	
	private PlayArea playArea;
	private LocalPlayer localPlayer;
	
	private FieldSelection selectionManager;
	private EffectCardNetworkSender sender;
	
	private MainView view;
	
	
	
	/**
	 * Saves the reference to the selection manager.
	 */
	
	FieldListener(Object theSynchronizer, PlayArea thePlayArea, LocalPlayer theLocalPlayer,
			FieldSelection theSelectionManager, EffectCardNetworkSender theSender, MainView theView) {
		synchronizer = theSynchronizer;
		
		playArea = thePlayArea;
		localPlayer = theLocalPlayer;
		
		selectionManager = theSelectionManager;
		sender = theSender;
		
		view = theView;
	}
	
	
	
	/**
	 * Implements MouseListener.mouseClicked().
	 */
	
	@Override
	public void mouseClicked(MouseEvent e) {
		synchronized (synchronizer) {
			if (playArea.currentTurnPlayer() != localPlayer.player()) {
				return;
			}
			
			// If no hand card is selected, it doesn't make sense to choose any targets.
			if (localPlayer.hand().selectedCardIndex() == -1) {
				return;
			}
			
			FieldLabel source = (FieldLabel)e.getSource();
			
			EffectCard card = localPlayer.hand().selectedCard();
			if (selectionManager.selectField(card, source.x(), source.y())) {
				if (selectionManager.targets().length == card.getNumberOfTargets()) {
					sender.playCard(localPlayer.hand().selectedCardIndex(), selectionManager.targets());
					localPlayer.hand().deselectCard();
					selectionManager.clearSelection();
				}
			}
			
			view.update();
		}
	}
	
	
	
	/**
	 * Implements MouseListener.mouseEntered().
	 */
	
	@Override
	public void mouseEntered(MouseEvent e) {
		((FieldLabel)e.getSource()).addTemporaryHighlight();
	}
	
	
	
	/**
	 * Implements MouseListener.mouseExited().
	 */
	
	@Override
	public void mouseExited(MouseEvent e) {
		((FieldLabel)e.getSource()).removeTemporaryHighlight();
	}
}
