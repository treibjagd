/*
	Copyright (c) 2007, 2008 Jacob Beller, Hanno Braun, Jan Lücker
	
	Permission to use, copy, modify, and/or distribute this software for any
	purpose with or without fee is hereby granted, provided that the above
	copyright notice and this permission notice appear in all copies.

	THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
	WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
	MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
	ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
	WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
	ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
	OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*/



package de.elrador.treibjagd.client.controller.main;



import de.elrador.treibjagd.client.game.LocalPlayer;
import de.elrador.treibjagd.client.game.FieldSelection;
import de.elrador.treibjagd.client.view.main.MainView;
import de.elrador.treibjagd.common.game.PlayArea;

import java.awt.event.MouseListener;

import ioproject.client.network.Server;



/**
 * Controls the playing of effect cards.
 */

public class EffectCardPlayController {
	
	private Object synchronizer;
	
	private PlayArea playArea;
	private LocalPlayer localPlayer;
	
	private EffectCardNetworkSender sender;
	private FieldSelection fieldSelection;
	
	private MainView view;
	
	
	
	/**
	 * Constructor.
	 */
	
	public EffectCardPlayController(Object theSynchronizer, Server server, PlayArea thePlayArea,
			LocalPlayer theLocalPlayer, MainView theView, FieldSelection theFieldSelection) {
		synchronizer = theSynchronizer;
		
		playArea = thePlayArea;
		localPlayer = theLocalPlayer;
		fieldSelection = theFieldSelection;
		
		sender = new EffectCardNetworkSender(server, playArea, localPlayer);
		
		
		view = theView;
	}
	
	
	
	/**
	 * Returns the field listener.
	 */
	
	public MouseListener fieldListener() {
		return new FieldListener(synchronizer, playArea, localPlayer, fieldSelection, sender, view);
	}
	
	
	
	/**
	 * Returns the hand listener.
	 */
	
	public MouseListener handListener() {
		return new HandListener(synchronizer, playArea, localPlayer, fieldSelection, sender, view);
	}
}
