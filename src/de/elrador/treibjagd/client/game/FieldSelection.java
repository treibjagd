/*
	Copyright (c) 2007, 2008 Jacob Beller, Hanno Braun, Jan Lücker
	
	Permission to use, copy, modify, and/or distribute this software for any
	purpose with or without fee is hereby granted, provided that the above
	copyright notice and this permission notice appear in all copies.

	THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
	WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
	MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
	ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
	WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
	ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
	OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*/



package de.elrador.treibjagd.client.game;



import de.elrador.treibjagd.common.game.PlayArea;
import de.elrador.treibjagd.common.game.cards.EffectCard;

import java.util.LinkedList;
import java.util.List;



/**
 * Manages the selection of effect cards and fields.
 * 
 * Attention: This class doesn't do any thread-synchronization. Please make sure all access to this class is
 * properly synchronized.
 */

public class FieldSelection {
	
	private PlayArea playArea;
	
	private List<EffectCard.Target> selectedFields;
	
	
	
	/**
	 * Constructor.
	 */
	
	public FieldSelection(PlayArea thePlayArea) {
		playArea = thePlayArea;
		
		selectedFields = new LinkedList<EffectCard.Target>();
	}
	
	
	
	/**
	 * Clears the selection.
	 */
	
	public void clearSelection() {
		selectedFields.clear();
	}
	
	
	
	/**
	 * Selects the field with the given position.
	 * 
	 * @return True if the field is a valid target and has been selected, false otherwise.
	 */
	
	public boolean selectField(EffectCard card, byte x, byte y) {
		if (card.getNumberOfTargets() == selectedFields.size()) {
			throw new IllegalStateException("Already selected enough targets.");
		}
		
		EffectCard.Target target = new EffectCard.Target(x, y);
		selectedFields.add(target);
		if (card.isTargetValid(playArea, selectedFields.toArray(new EffectCard.Target[] {}))) {
			return true;
		}
		else {
			selectedFields.remove(target);
			return false;
		}
	}
	
	
	
	/**
	 * Returns the selected targets as an array.
	 */
	
	public EffectCard.Target[] targets() {
		return selectedFields.toArray(new EffectCard.Target[] {});
	}
}
