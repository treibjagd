/*
	Copyright (c) 2007, 2008 Jacob Beller, Hanno Braun, Jan Lücker
	
	Permission to use, copy, modify, and/or distribute this software for any
	purpose with or without fee is hereby granted, provided that the above
	copyright notice and this permission notice appear in all copies.

	THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
	WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
	MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
	ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
	WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
	ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
	OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*/



package de.elrador.treibjagd.client.game;



import de.elrador.treibjagd.common.game.cards.EffectCard;



/**
 * Manages the cards of the local player.
 */

public class Hand {
	
	private EffectCard[] cards;
	private byte selectedCardIndex;
	
	
	
	/**
	 * Initializes the cards.
	 */
	
	Hand() {
		cards = new EffectCard[4];
		selectedCardIndex = -1;
	}
	
	
	
	/**
	 * Returns a card from the cards.
	 */
	
	public EffectCard card(byte index) {
		return cards[index];
	}
	
	
	
	/**
	 * Sets a card in the hand.
	 */
	
	public void setCard(byte index, EffectCard card) {
		cards[index] = card;
	}
	
	
	
	/**
	 * Returns the size of the hand.
	 */
	
	public int size() {
		return cards.length;
	}
	
	
	
	/**
	 * Selects the card with the given index.
	 */
	
	public void selectCard(byte index) {
		selectedCardIndex = index;
	}
	
	
	
	/**
	 * Deselects the card.
	 */
	
	public void deselectCard() {
		if (selectedCardIndex == -1) {
			throw new IllegalStateException("No card selected.");
		}
		
		selectedCardIndex = -1;
	}
	
	
	
	/**
	 * Returns the index of the selected card.
	 */
	
	public byte selectedCardIndex() {
		return selectedCardIndex;
	}
	
	
	
	/**
	 * Returns the selected card.
	 */
	
	public EffectCard selectedCard() {
		return cards[selectedCardIndex];
	}
}
