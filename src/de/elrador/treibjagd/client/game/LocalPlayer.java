/*
	Copyright (c) 2007, 2008 Jacob Beller, Hanno Braun, Jan Lücker
	
	Permission to use, copy, modify, and/or distribute this software for any
	purpose with or without fee is hereby granted, provided that the above
	copyright notice and this permission notice appear in all copies.

	THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
	WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
	MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
	ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
	WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
	ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
	OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*/



package de.elrador.treibjagd.client.game;



import de.elrador.treibjagd.common.game.Player;
import de.elrador.treibjagd.common.game.cards.*;



/**
 * Holds all data and functionality for the client-side representation of the local player.
 */

public class LocalPlayer {
	
	private Player player;
	private Hand hand;
	
	private int effectCardsOnStack;
	
	
	/**
	 * Creates a new LocalPlayer object around a Player object.
	 */
	
	public LocalPlayer(Player aPlayer) {
		player = aPlayer;
		hand = new Hand();
		
		effectCardsOnStack = new EffectCardLoader().getCards(player.race()).size();
	}
	
	
	
	/**
	 * Returns the encapsulated player.
	 */
	
	public Player player() {
		return player;
	}
	
	
	
	/**
	 * Returns the player's hand.
	 */
	
	public Hand hand() {
		return hand;
	}
	
	
	
	/**
	 * Returns the number of effect cards on this player's stack.
	 */
	
	public int effectCardsOnStack() {
		return effectCardsOnStack;
	}
	
	
	
	/**
	 * Decrease the number of effect cards.
	 */
	
	public void decreaseEffectCards() {
		effectCardsOnStack -= 1;
	}
}
