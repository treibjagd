/*
	Copyright (c) 2007, 2008 Jacob Beller, Hanno Braun, Jan Lücker
	
	Permission to use, copy, modify, and/or distribute this software for any
	purpose with or without fee is hereby granted, provided that the above
	copyright notice and this permission notice appear in all copies.

	THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
	WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
	MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
	ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
	WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
	ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
	OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*/



package de.elrador.treibjagd.client.view.game;



import de.elrador.treibjagd.client.view.ViewUtils;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;



/**
 * Manages the game management GUI.
 */

public class GameView implements ActionListener {
	
	private GameFrame frame;
	private GameListener listener;
	
	
	
	/**
	 * Constructor.
	 */
	
	public GameView() {
		final GameView gameView = this;
		ViewUtils.executeSafe(new Runnable() {
			public void run() {
				frame = new GameFrame();
				frame.startButton.addActionListener(gameView);
			}
		});
	}
	
	
	
	/**
	 * Adds a listener that will be notified if the user attempts to start the game.
	 */
	
	public void addGameListener(GameListener theListener) {
		listener = theListener;
	}
	
	
	
	/**
	 * Sets frame's visibility.
	 */
	
	public void setVisible(final boolean visible) {
		ViewUtils.executeSafe(new Runnable() {
			public void run() {
				frame.setVisible(visible);
			}
		});
	}
	
	
	
	/**
	 * Disables the start button.
	 */
	
	public void disable() {
		ViewUtils.executeSafe(new Runnable() {
			public void run() {
				frame.startButton.setEnabled(false);
			}
		});
	}
	
	
	
	/**
	 * Reacts to the button action.
	 */
	
	public void actionPerformed(ActionEvent e) {
		listener.startRequested();
	}
}
