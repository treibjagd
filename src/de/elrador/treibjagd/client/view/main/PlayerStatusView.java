/*
	Copyright (c) 2007, 2008 Jacob Beller, Hanno Braun, Jan Lücker
	
	Permission to use, copy, modify, and/or distribute this software for any
	purpose with or without fee is hereby granted, provided that the above
	copyright notice and this permission notice appear in all copies.

	THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
	WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
	MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
	ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
	WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
	ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
	OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*/



package de.elrador.treibjagd.client.view.main;



import java.awt.Color;
import java.awt.GridLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.LineBorder;



/**
 * Displays the status of a single player.
 */

class PlayerStatusView {
	
	private JPanel panel;
	
	private JLabel name;
	private JLabel score;
	private JLabel animals;
	
	
	
	/**
	 * Creates a new PlayerStatusView instance.
	 * 
	 * @param panel The panel this view uses to display information.
	 */
	
	PlayerStatusView(JPanel thePanel, Color color) {
		panel = thePanel;
		
		panel.setBorder(new LineBorder(new Color(0, 0, 0), 1, false));
		
		panel.setLayout(new GridLayout(3, 2));
		
		name = new JLabel("-");
		score = new JLabel("-");
		animals = new JLabel("-");
		
		JLabel colorLabel = new JLabel();
		colorLabel.setOpaque(true);
		colorLabel.setBackground(color);
		
		panel.add(name);
		panel.add(new JLabel());
		panel.add(colorLabel);
		panel.add(new JLabel("Punkte: "));
		panel.add(score);
		panel.add(new JLabel());
		panel.add(new JLabel("Tiere: "));
		panel.add(animals);
	}
	
	
	/**
	 * Updates the displayed information.
	 * May be called from the Swing thread. 
	 */
	
	void update(String newName, Integer newScore, Integer newAnimals) {
		name.setText(newName);
		score.setText(newScore.toString());
		animals.setText(newAnimals.toString());
	}
	
	
	
	/**
	 * Sets the border of the status view to highlight it or return it to normal.
	 */
	
	void highlight(boolean highlight) {
		if (highlight) {
			panel.setBorder(new LineBorder(new Color(255, 0, 0), 2, false));
		}
		else {
			panel.setBorder(new LineBorder(new Color(0, 0, 0), 1, false));
		}
	}
}
