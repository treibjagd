/*
	Copyright (c) 2007, 2008 Jacob Beller, Hanno Braun, Jan Lücker
	
	Permission to use, copy, modify, and/or distribute this software for any
	purpose with or without fee is hereby granted, provided that the above
	copyright notice and this permission notice appear in all copies.

	THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
	WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
	MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
	ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
	WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
	ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
	OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*/



package de.elrador.treibjagd.client.view.main;



import de.elrador.treibjagd.client.game.FieldSelection;
import de.elrador.treibjagd.common.game.Field;
import de.elrador.treibjagd.common.game.PlayArea;

import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.MouseListener;
import javax.swing.ImageIcon;
import javax.swing.JPanel;



/**
 * Displays the field used for playing.
 */

public class FieldView {
	
	private FieldLabel[][] fields;
	
	private PlayArea playArea;
	
	
	
	/**
	 * Must be called from the Swing thread.
	 */
	
	public FieldView(JPanel panel, PlayArea thePlayArea, FieldSelection fieldSelection) {
		playArea = thePlayArea;
		
		panel.setLayout(new GridLayout(3, 5));
		
		fields = new FieldLabel[5][3];
		for (byte j = 0; j < fields[0].length; j++) {
			for (byte i = 0; i < fields.length; i++) {
				fields[i][j] = new FieldLabel(i, j, fieldSelection);
				fields[i][j].setPreferredSize(new Dimension(110, 80));
				panel.add(fields[i][j]);
			}
		}
		
		panel.setOpaque(true);
	}
	
	
	
	/**
	 * Updates the view.
	 */
	
	void update() {
		for (int i = 0; i < fields.length; i++) {
			for (int j = 0; j < fields[0].length; j++) {
				Field field = playArea.field(i, j);
				fields[i][j].setIcon(new ImageIcon(field.getImage()));
				fields[i][j].update();
			}
		}
	}
	
	
	
	/**
	 * Adds the listener to all field labels.
	 */
	
	void addListener(MouseListener listener) {
		for (byte j = 0; j < fields[0].length; j++) {
			for (byte i = 0; i < fields.length; i++) {
				fields[i][j].addMouseListener(listener);
			}
		}
	}
}
