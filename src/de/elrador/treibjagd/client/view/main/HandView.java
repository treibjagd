/*
	Copyright (c) 2007, 2008 Jacob Beller, Hanno Braun, Jan Lücker
	
	Permission to use, copy, modify, and/or distribute this software for any
	purpose with or without fee is hereby granted, provided that the above
	copyright notice and this permission notice appear in all copies.

	THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
	WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
	MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
	ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
	WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
	ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
	OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*/



package de.elrador.treibjagd.client.view.main;



import de.elrador.treibjagd.client.game.*;

import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.MouseListener;
import javax.swing.*;



/**
 * Displays the effect cardLabels that are available to the player.
 */

public class HandView {
	
	private HandLabel[] cardLabels;
	private JLabel lastCard;
	
	private LocalPlayer localPlayer;
	
	private LastPlayedCard lastPlayedCard;
	
	
	
	/**
	 * Creates a new instance of this class.
	 * This constructor must be called from the Swing thread.
	 * 
	 * @param thePanel The panel to draw on.
	 */
	
	public HandView(JPanel panel, LocalPlayer theLocalPlayer, LastPlayedCard theLastPlayedCard) {
		localPlayer = theLocalPlayer;
		lastPlayedCard = theLastPlayedCard;
		
		panel.setLayout(new GridLayout(1, 6));
		
		cardLabels = new HandLabel[4];
		for (byte i = 0; i < cardLabels.length; i++) {
			cardLabels[i] = new HandLabel(i, localPlayer.hand());
			cardLabels[i].setPreferredSize(new Dimension(100, 170));
			panel.add(cardLabels[i]);
		}
		
		lastCard = new JLabel();
		lastCard.setPreferredSize(new Dimension(100, 170));
		panel.add(lastCard);
		
		panel.setOpaque(true);
	}
	
	
	
	/**
	 * Updates the view.
	 */
	
	void update() {
		for (HandLabel label : cardLabels) {
			label.update();
		}
		
		if (lastPlayedCard.getCard() != null) {
			String path = lastPlayedCard.getCard().graphicsPath();
			lastCard.setIcon(new ImageIcon("grafik/cards/" + path));
		}
		else {
			lastCard.setIcon(new ImageIcon(HandLabel.NOCARD));
		}
	}
	
	
	
	/**
	 * Adds the listener to all labels.
	 */
	
	void addListener(MouseListener listener) {
		for (byte i = 0; i < cardLabels.length; i++) {
			cardLabels[i].addMouseListener(listener);
		}
	}
}
