/*
	Copyright (c) 2007, 2008 Jacob Beller, Hanno Braun, Jan Lücker
	
	Permission to use, copy, modify, and/or distribute this software for any
	purpose with or without fee is hereby granted, provided that the above
	copyright notice and this permission notice appear in all copies.

	THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
	WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
	MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
	ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
	WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
	ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
	OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*/



package de.elrador.treibjagd.client.view.main;



import de.elrador.treibjagd.client.game.*;
import de.elrador.treibjagd.common.game.PlayArea;
import de.elrador.treibjagd.common.game.Player;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.LineBorder;



/**
 * Controls the status panel.
 */

public class StatusView {
	
	private static final Color[] PLAYER_COLORS = new Color[] {new Color(64, 64, 64),
	                                                          new Color(0, 255, 255),
	                                                          new Color(127, 106, 0),
	                                                          new Color(255, 0, 220),
	                                                          new Color(255, 61, 12),
	                                                          new Color(255, 255, 255)};
	
	private JPanel panel;
	
	private PlayerStatusView[] playerStatusViews;
	private PlayerStatusView highlightedView;
	
	private JLabel reinforcementCards;
	private JLabel effectCards;
	
	private PlayArea playArea;
	private LocalPlayer localPlayer;
	
	
	
	/**
	 * Creates a new instance of this class.
	 * This constructor must be called from the Swing thread.
	 * 
	 * @param panel The JPanel that this class is supposed to use for displaying the information.
	 */
	
	public StatusView(JPanel thePanel, PlayArea thePlayArea, LocalPlayer theLocalPlayer) {
		panel = thePanel;
		playArea = thePlayArea;
		localPlayer = theLocalPlayer;
		highlightedView = null;
		
		panel.setLayout(new BorderLayout());
		
		JPanel playerStatusPanel = new JPanel();
		playerStatusPanel.setPreferredSize(new Dimension(155, 400));
		playerStatusPanel.setLayout(new GridLayout(6, 1));
		
		playerStatusViews = new PlayerStatusView[6];
		for (byte i = 0; i < playerStatusViews.length; i++) {
			JPanel singlePlayerStatusPanel = new JPanel();
			
			playerStatusViews[i] = new PlayerStatusView(singlePlayerStatusPanel, PLAYER_COLORS[i]);
			playerStatusPanel.add(singlePlayerStatusPanel);
		}
		
		reinforcementCards = new JLabel("-");
		effectCards = new JLabel("-");
		
		JPanel statusPanel = new JPanel();
		statusPanel.setBorder(new LineBorder(new Color(0, 0, 0), 1, false));
		statusPanel.setLayout(new GridLayout(7, 1));
		statusPanel.add(new JLabel("Verbleibende"));
		statusPanel.add(new JLabel("Nachschubkarten:"));
		statusPanel.add(reinforcementCards);
		statusPanel.add(new JLabel());
		statusPanel.add(new JLabel("Verbleibende"));
		statusPanel.add(new JLabel("Effektkarten:"));
		statusPanel.add(effectCards);
		
		panel.add(playerStatusPanel, BorderLayout.NORTH);
		panel.add(statusPanel, BorderLayout.CENTER);
	}
	
	
	
	/**
	 * Updates the view.
	 */
	
	void update() {
		byte i = 0;
		for (Player player : playArea.players()) {
			playerStatusViews[i++].update(player.name(), player.score(), player.animalsLeft());
		}
		
		// Highlight the player whose turn it currently is.
		Player currentTurnPlayer = playArea.currentTurnPlayer();
		if (currentTurnPlayer != null) {
			PlayerStatusView currentTurnPlayerView = playerStatusViews[currentTurnPlayer.id()];
			if (currentTurnPlayerView != highlightedView) {
				// Remove highlight from the previous view.
				if (highlightedView != null) {
					highlightedView.highlight(false);
				}
				
				// Highlight the current view.
				currentTurnPlayerView.highlight(true);
				highlightedView = currentTurnPlayerView;
			}
		}
		
		// Show the number of cards left on the reinforcement stack.
		reinforcementCards.setText(new Integer(playArea.cardsOnReinforcementStack()).toString());
		
		// Show the number of effect cards left.
		effectCards.setText(new Integer(localPlayer.effectCardsOnStack()).toString());
	}
}

