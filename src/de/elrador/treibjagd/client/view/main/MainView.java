/*
	Copyright (c) 2007, 2008 Jacob Beller, Hanno Braun, Jan Lücker
	
	Permission to use, copy, modify, and/or distribute this software for any
	purpose with or without fee is hereby granted, provided that the above
	copyright notice and this permission notice appear in all copies.

	THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
	WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
	MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
	ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
	WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
	ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
	OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*/



package de.elrador.treibjagd.client.view.main;



import de.elrador.treibjagd.client.game.*;
import de.elrador.treibjagd.client.view.ViewUtils;
import de.elrador.treibjagd.common.game.PlayArea;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseListener;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;



/**
 * Stellt das Hauptfenster dar.
 */

public class MainView {
	
	private JFrame frame;
	
	private StatusView statusView;
	private HandView handView;
	private FieldView fieldView;
	
	private PlayArea playArea;
	private LocalPlayer localPlayer;
	
	
	
	/**
	 * Creates the frame and all panels.
	 * Must be called from the swing thread.
	 */
	
	public MainView(final PlayArea thePlayArea, final LocalPlayer theLocalPlayer,
			final FieldSelection fieldSelection, final LastPlayedCard lastPlayedCard) {
		ViewUtils.executeSafe(new Runnable() {
			public void run() {
				playArea = thePlayArea;
				localPlayer = theLocalPlayer;
				
				// Main window
				frame = new JFrame("Treibjagd - " + localPlayer.player().name());
				frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				
				// The menu
				JMenuBar menuBar = new JMenuBar();
				menuBar.setPreferredSize(new Dimension(800, 20));
				JMenu treibjagdMenu = new JMenu("Treibjagd");
				JMenuItem exitItem = new JMenuItem("Beenden");
				exitItem.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						System.exit(0);
					}
				});
				treibjagdMenu.add(exitItem);
				menuBar.add(treibjagdMenu);
				
				// Add a panel for the main field.
				JPanel fieldPanel = new JPanel();
				fieldPanel.setPreferredSize(new Dimension(645, 370));
				fieldView = new FieldView(fieldPanel, playArea, fieldSelection);
				
				// Add a panel for the cards.
				JPanel cardPanel = new JPanel();
				cardPanel.setPreferredSize(new Dimension(645, 180));
				handView = new HandView(cardPanel, localPlayer, lastPlayedCard);
				
				// Panel for the main and card fields.
				JPanel fieldCardPanel = new JPanel();
				fieldCardPanel.setLayout(new BorderLayout());
				fieldCardPanel.add(fieldPanel, BorderLayout.NORTH);
				fieldCardPanel.add(cardPanel, BorderLayout.SOUTH);
				
				// Add a panel for the player statistics.
				JPanel statusPanel = new JPanel();
				statusPanel.setPreferredSize(new Dimension(155, 540));
				statusView = new StatusView(statusPanel, playArea, localPlayer);
				
				// Add components to the main window, pack and make visible.
				frame.setJMenuBar(menuBar);
				frame.getContentPane().add(fieldCardPanel, BorderLayout.WEST);
				frame.getContentPane().add(statusPanel, BorderLayout.EAST);
				frame.pack();
			}
		});
	}
	
	
	
	/**
	 * Sets the main window visible.
	 */
	
	public void setVisible() {
		ViewUtils.executeSafe(new Runnable() {
			public void run() {
				frame.setVisible(true);
			}
		});
	}
	
	
	
	/**
	 * Updates the view and its sub-views.
	 */
	
	public void update() {
		ViewUtils.executeSafe(new Runnable() {
			public void run() {
				handView.update();
				fieldView.update();
				statusView.update();
			}
		});
	}
	
	
	
	/**
	 * Adds the listeners.
	 */
	
	public void addListeners(final MouseListener handListener, final MouseListener fieldListener) {
		ViewUtils.executeSafe(new Runnable() {
			public void run() {
				fieldView.addListener(fieldListener);
				handView.addListener(handListener);
			}
		});
	}
}
