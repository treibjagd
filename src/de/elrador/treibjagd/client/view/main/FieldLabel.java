/*
	Copyright (c) 2007, 2008 Jacob Beller, Hanno Braun, Jan Lücker
	
	Permission to use, copy, modify, and/or distribute this software for any
	purpose with or without fee is hereby granted, provided that the above
	copyright notice and this permission notice appear in all copies.

	THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
	WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
	MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
	ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
	WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
	ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
	OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*/



package de.elrador.treibjagd.client.view.main;



import de.elrador.treibjagd.client.game.FieldSelection;
import de.elrador.treibjagd.common.game.cards.EffectCard;

import java.awt.Color;
import javax.swing.JLabel;
import javax.swing.border.LineBorder;



/**
 * Used for displaying fields.
 */

public class FieldLabel extends JLabel {
	
	private static final LineBorder NO_HIGHLIGHT = new LineBorder(Color.YELLOW, 0, false);
	private static final LineBorder TEMPORARY_HIGHLIGHT = new LineBorder(Color.YELLOW, 3, false);
	private static final LineBorder HIGHLIGHT = new LineBorder(Color.BLUE, 3, false);
	
	private byte x;
	private byte y;
	
	private FieldSelection selection;
	
	private LineBorder currentHighlight;
	
	
	
	/**
	 * Saves the field coordinates.
	 */
	
	FieldLabel(byte xPos, byte yPos, FieldSelection theSelection) {
		x = xPos;
		y = yPos;
		
		selection = theSelection;
		
		currentHighlight = NO_HIGHLIGHT;
	}
	
	
	
	/**
	 * Returns x.
	 */
	
	public byte x() {
		return x;
	}
	
	
	
	/**
	 * Returns y.
	 */
	
	public byte y() {
		return y;
	}
	
	
	
	/**
	 * Adds a temporary highlight.
	 */
	
	public void addTemporaryHighlight() {
		setBorder(TEMPORARY_HIGHLIGHT);
	}
	
	
	
	/**
	 * Removes the temporary highlight.
	 */
	
	public void removeTemporaryHighlight() {
		setBorder(currentHighlight);
	}
	
	
	
	/**
	 * Updates the selection markings.
	 */
	
	void update() {
		EffectCard.Target[] targets = selection.targets();
		for (EffectCard.Target target : targets) {
			if (target.x == x && target.y == y) {
				currentHighlight = HIGHLIGHT;
				setBorder(HIGHLIGHT);
				return;
			}
		}
		
		currentHighlight = NO_HIGHLIGHT;
		setBorder(NO_HIGHLIGHT);
	}
}
