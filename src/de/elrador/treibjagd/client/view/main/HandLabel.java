/*
	Copyright (c) 2007, 2008 Jacob Beller, Hanno Braun, Jan Lücker
	
	Permission to use, copy, modify, and/or distribute this software for any
	purpose with or without fee is hereby granted, provided that the above
	copyright notice and this permission notice appear in all copies.

	THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
	WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
	MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
	ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
	WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
	ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
	OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*/



package de.elrador.treibjagd.client.view.main;



import de.elrador.treibjagd.client.game.Hand;

import java.awt.Color;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.border.LineBorder;



/**
 * Displays card on the hand.
 */

public class HandLabel extends JLabel {
	
	public static final String NOCARD = "grafik/cards/nocard.png";
	
	private static final LineBorder NO_HIGHLIGHT = new LineBorder(Color.YELLOW, 0, false);
	private static final LineBorder TEMPORARY_HIGHLIGHT = new LineBorder(Color.YELLOW, 3, false);
	private static final LineBorder HIGHLIGHT = new LineBorder(Color.BLUE, 3, false);
	
	private byte index;
	private Hand hand;
	
	private LineBorder currentHighlight;
	
	
	
	/**
	 * Sets the index of the card this label displays.
	 */
	
	HandLabel(byte theIndex, Hand theHand) {
		index = theIndex;
		hand = theHand;
		
		currentHighlight = NO_HIGHLIGHT;
		
		setIcon(new ImageIcon(NOCARD));
	}
	
	
	
	/**
	 * Returns the index.
	 */
	
	public byte index() {
		return index;
	}
	
	
	
	/**
	 * Adds a temporary highlight to the label.
	 */
	
	public void addTemporaryHighlight() {
		setBorder(TEMPORARY_HIGHLIGHT);
	}
	
	
	
	/**
	 * Removes the temporary highlight.
	 */
	
	public void removeTemporaryHighlight() {
		setBorder(currentHighlight);
	}
	
	
	
	/**
	 * Updates the Label.
	 */
	
	void update() {
		if (hand.card(index) != null) {
			String path = hand.card(index).graphicsPath();
			setIcon(new ImageIcon("grafik/cards/" + path));
		}
		else {
			setIcon(new ImageIcon(NOCARD));
		}
		
		if (hand.selectedCardIndex() == index) {
			setBorder(HIGHLIGHT);
			currentHighlight = HIGHLIGHT;
		}
		else {
			setBorder(NO_HIGHLIGHT);
			currentHighlight = NO_HIGHLIGHT;
		}
	}
}
