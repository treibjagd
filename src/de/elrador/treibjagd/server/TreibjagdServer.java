/*
	Copyright (c) 2007, 2008 Jacob Beller, Hanno Braun, Jan Lücker
	
	Permission to use, copy, modify, and/or distribute this software for any
	purpose with or without fee is hereby granted, provided that the above
	copyright notice and this permission notice appear in all copies.

	THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
	WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
	MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
	ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
	WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
	ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
	OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*/



package de.elrador.treibjagd.server;



import de.elrador.treibjagd.common.game.Race;
import de.elrador.treibjagd.common.network.LastMessageInvalidMessage;
import de.elrador.treibjagd.common.network.gamemanagement.JoinGameMessage;
import de.elrador.treibjagd.common.network.init.NameAcceptedMessage;
import de.elrador.treibjagd.common.network.init.SetNameMessage;
import de.elrador.treibjagd.server.game.GameClientHandler;
import de.elrador.treibjagd.server.game.TreibjagdGame;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.HashSet;
import java.util.Set;

import ioproject.common.network.Node;
import ioproject.server.network.Client;
import ioproject.server.network.ClientGroup;
import ioproject.server.network.ClientHandler;
import ioproject.server.network.NetworkService;



/**
 * Serverseitige Hauptklasse.
 */

public class TreibjagdServer implements ClientHandler, Runnable {
	
	public enum SessionState {LOGGED_IN, IN_LOBBY, IN_GAME}
	
	private Thread thread;
	
	private NetworkService net;
	
	private ClientGroup allClients;
	private Set<String> names;
	
	private ClientGroup gameGroup;
	
	private TreibjagdGame game;
	
	
	
	/**
	 * Saves a reference to the comm service.
	 */
	
	public TreibjagdServer() {
		net = new NetworkService();
		
		names = new HashSet<String>();
		
		game = new TreibjagdGame();
		
		// Retrieve the global client group from the network service and register this object as handler.
		allClients = net.globalClientGroup();
		allClients.addClientHandler(this);
		
		gameGroup = allClients.createSubGroup();
		new GameClientHandler(game, gameGroup);
		
		try {
			net.connect(new InetSocketAddress(34480));
		}
		catch (IOException e) {
			System.out.println(e.toString());
			System.exit(1);
		}
	}
	
	
	
	/**
	 * Implementiert Game.run().
	 */

	public void run() {
		thread = Thread.currentThread();
		
		System.out.println("Treibjagd Server, Version 0.1-dev");
		System.out.println("Ein Produkt des Elrador-Projekts. http://elrador.de");
		System.out.println("System gestartet.");
		
		synchronized (thread) {
			try {
				thread.wait();
			}
			catch (InterruptedException e) {
				thread.interrupt();
			}
		}
			
		System.out.println("Beendet.");
	}
	
	
	
	/**
	 * Implements ClientHandler.clientAdded().
	 * Sets the new client's state.
	 */
	
	public synchronized void clientAdded(Client client) {
		client.attribute("state", SessionState.LOGGED_IN);
		System.out.println("Client akzeptiert (" + client + ").");
	}
	
	
	
	/**
	 * Implements ClientHandler.clientRemoved().
	 * Removes the client from the client set and, if the corresponding player participates in a game,
	 * notifies the game that the player has left.
	 */
	
	public synchronized void clientRemoved(Client client) {
		SessionState sessionState = (SessionState)client.attribute("state");
		
		// If the player is in a game he has to be removed.
		if (sessionState == SessionState.IN_GAME) {
			// Nothing to do here anymore. Everything necessary is done by GameClientHandler.
			// This block is to be removed when the refactoring (to make better use of the new group feature
			// of the framework) is finished.
		}
		
		// If the player is in the lobby, he his name has to be freed.
		if (sessionState == SessionState.IN_LOBBY) {
			names.remove((String)client.attribute("name"));
		}
		
		String name = (String)client.attribute("name"); 
		if (name != null) {
			names.remove(name);
		}
		
		System.out.println("Clientverbindung geschlossen (" + client + ").");
	}
	
	
	
	/**
	 * Implements ClientHandler.messageSent().
	 */
	
	public void messageSent(Node client, Object message) {
		// Not interested in outgoing messages.
	}
	
	
	
	/**
	 * Implements ClientHandler.messageReceived().
	 * Processes the incoming message.
	 */
	
	public synchronized void messageReceived(Node node, Object message) {
		Client client = (Client)node;
		SessionState sessionState = (SessionState)client.attribute("state");
		if (sessionState == SessionState.LOGGED_IN) {
			if (message instanceof SetNameMessage) {
				String name = ((SetNameMessage)message).name;
				if (!names.contains(name)) {
					names.add(name);
					client.attribute("name", name);
					client.attribute("state", SessionState.IN_LOBBY);
					client.send(new NameAcceptedMessage(true));
				}
				else {
					client.send(new NameAcceptedMessage(false));
				}
			}
			else {
				allClients.remove(client);
				client.send(new LastMessageInvalidMessage());
				client.disconnect();
			}
		}
		else if (sessionState == SessionState.IN_LOBBY) {
			if (message instanceof JoinGameMessage) {
				try {
					Race race = Race.raceFromByte(((JoinGameMessage)message).race);
					client.attribute("race", race);
					gameGroup.add(client);
				}
				catch (IllegalArgumentException e) {
					// If the race id was invalid, this is what will happen (thrown by raceFromByte()).
					client.send(new LastMessageInvalidMessage());
					client.disconnect();
				}
			}
			else {
				client.send(new LastMessageInvalidMessage());
				client.disconnect();
			}
		}
		else if (sessionState == SessionState.IN_GAME) {
			// Nothing to do here anymore. Everything necessary is done by GameClientHandler.
			// This block is to be removed when the refactoring (to make better use of the new group feature
			// of the framework) is finished.
		}
	}
	
	
	
	/**
	 * Implements ClientHandler.exceptionCaught().
	 * Disconnects the client that is responsible for the exception and prints out a stack trace.
	 */
	
	public void exceptionCaught(Client client, Throwable cause) {
		client.disconnect();
		System.out.println("Fehler (Session (" + client + "):");
		cause.printStackTrace();
	}
	
	
	
	/**
	 * Implementiert Game.shutDown().
	 */

	public void shutDown() {
		synchronized (thread) {
			try {
				thread.interrupt();
				thread.join();
			}
			catch (InterruptedException e) {
				assert false : "This should never happen.";
			}
		}
	}
}

