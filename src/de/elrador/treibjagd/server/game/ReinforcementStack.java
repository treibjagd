/*
	Copyright (c) 2007, 2008 Jacob Beller, Hanno Braun, Jan Lücker
	
	Permission to use, copy, modify, and/or distribute this software for any
	purpose with or without fee is hereby granted, provided that the above
	copyright notice and this permission notice appear in all copies.

	THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
	WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
	MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
	ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
	WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
	ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
	OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*/



package de.elrador.treibjagd.server.game;



import de.elrador.treibjagd.common.game.FieldCard;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;



/**
 * The reinforcement stack.
 */

public class ReinforcementStack {
	
	private List<FieldCard> stack;
	
	
	
	/**
	 * Creates the internal list for the reinforcement cards.
	 */
	
	ReinforcementStack() {
		stack = new LinkedList<FieldCard>();
	}
	
	
	
	/**
	 * Adds the given cards to the stack and shuffles the stack.
	 */
	
	void addCardsAndShuffle(List<FieldCard> cards) {
		stack.addAll(cards);
		Collections.shuffle(stack);
	}
	
	
	
	/**
	 * Adds the given card to the top of the stack.
	 */
	
	void addCardOnTop(FieldCard card) {
		stack.add(0, card);
	}
	
	
	
	/**
	 * Adds a card at the bottom of the stack.
	 */
	
	void addAtBottom(FieldCard card) {
		stack.add(stack.size(), card);
	}
	
	
	
	/**
	 * Returns the first card from the stack. If the stack is empty, this returns a new emtpy-field-card.
	 */
	
	FieldCard getCard() {
		if (!stack.isEmpty()) {
			return stack.remove(0);
		}
		else {
			return new FieldCard(FieldCard.Type.EMPTY);
		}
	}
}
