/*
	Copyright (c) 2007, 2008 Jacob Beller, Hanno Braun, Jan Lücker
	
	Permission to use, copy, modify, and/or distribute this software for any
	purpose with or without fee is hereby granted, provided that the above
	copyright notice and this permission notice appear in all copies.

	THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
	WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
	MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
	ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
	WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
	ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
	OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*/



package de.elrador.treibjagd.server.game;



import de.elrador.treibjagd.common.game.Race;
import de.elrador.treibjagd.common.network.LastMessageInvalidMessage;
import de.elrador.treibjagd.common.network.gamemanagement.JoinedGameMessage;
import de.elrador.treibjagd.common.network.ingame.DropCardMessage;
import de.elrador.treibjagd.common.network.ingame.PlayEffectCardMessage;
import de.elrador.treibjagd.common.network.ingame.StartGameMessage;
import de.elrador.treibjagd.server.TreibjagdServer.SessionState;

import ioproject.common.network.Node;
import ioproject.server.network.Client;
import ioproject.server.network.ClientGroup;
import ioproject.server.network.ClientHandler;



/**
 * Handler for clients in the game ClientGroup.
 */

public class GameClientHandler implements ClientHandler {
	
	private TreibjagdGame game;
	private ClientGroup gameGroup;
	
	
	
	/**
	 * Constructor.
	 */
	
	public GameClientHandler(TreibjagdGame theGame, ClientGroup theGameGroup) {
		game = theGame;
		gameGroup = theGameGroup;
		
		gameGroup.addClientHandler(this);
	}
	
	
	
	/**
	 * Implements ClientHandler.clientAdded().
	 */
	
	public synchronized void clientAdded(Client client) {
		Race race = (Race)client.attribute("race");
		if (game.addPlayer(client, race)) {
			client.attribute("state", SessionState.IN_GAME);
			client.send(new JoinedGameMessage(true));
		}
		else {
			client.send(new JoinedGameMessage(false));
			gameGroup.remove(client);
		}
	}
	
	
	
	/**
	 * Implements ClientHandler.clientRemoved().
	 */
	
	public synchronized void clientRemoved(Client client) {
		game.removePlayer(client);
		client.attribute("state", SessionState.IN_LOBBY);
	}
	
	
	
	/**
	 * Implements ClientHandler.messageSent().
	 */
	
	public synchronized void messageSent(Node client, Object message) {
		// Not interested in outgoing messages.
	}
	
	
	
	/**
	 * Implements ClientHandler.messageReceived().
	 */
	
	public synchronized void messageReceived(Node node, Object message) {
		Client client = (Client)node;
		
		if (message instanceof StartGameMessage) {
			game.start(); // boolean return value ignored
		}
		else if (message instanceof DropCardMessage) {
			if (!game.playerDroppedCard(client, ((DropCardMessage)message).index)) {
				client.send(new LastMessageInvalidMessage());
				client.disconnect();
			}
		}
		else if (message instanceof PlayEffectCardMessage) {
			PlayEffectCardMessage playMessage = (PlayEffectCardMessage)message;
			if (!game.playerPlayedCard(client, playMessage.slotInHandId, playMessage.targets())) {
				client.send(new LastMessageInvalidMessage());
				client.disconnect();
			}
		}
		else {
			client.send(new LastMessageInvalidMessage());
			client.disconnect();
		}
	}
	
	
	
	/**
	 * Implements ClientHandler.exceptionCaught().
	 */
	
	public synchronized void exceptionCaught(Client client, Throwable cause) {
		client.disconnect();
		cause.printStackTrace();
	}
}
