/*
	Copyright (c) 2007, 2008 Jacob Beller, Hanno Braun, Jan Lücker
	
	Permission to use, copy, modify, and/or distribute this software for any
	purpose with or without fee is hereby granted, provided that the above
	copyright notice and this permission notice appear in all copies.

	THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
	WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
	MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
	ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
	WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
	ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
	OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*/



package de.elrador.treibjagd.server.game;



import de.elrador.treibjagd.common.game.*;
import de.elrador.treibjagd.common.game.cards.EffectCard;
import de.elrador.treibjagd.common.game.cards.EffectCardLoader;
import de.elrador.treibjagd.common.network.ingame.CardFromReinforcementStackMessage;
import de.elrador.treibjagd.common.network.ingame.EffectCardPlayedMessage;
import de.elrador.treibjagd.common.network.ingame.GameEndsMessage;
import de.elrador.treibjagd.common.network.ingame.GameStartedMessage;
import de.elrador.treibjagd.common.network.ingame.TurnChangeMessage;

import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import ioproject.server.network.Client;



/**
 * Holds all data for a single Treibjagd game and provides methods to act upon this data.
 */

public class TreibjagdGame implements NewCardNeededListener, GameConstants {
	
	private Map<Client, RemotePlayer> players;
	private boolean started;
	
	private EffectCardLoader cardLoader;
	
	private byte nextId;
	
	private PlayArea playArea;
	
	private ReinforcementStack reinforcementStack;
	
	
	
	/**
	 * Constructor.
	 */
	
	public TreibjagdGame() {		
		players = new HashMap<Client, RemotePlayer>();
		started = false;
		
		cardLoader = new EffectCardLoader();
		
		nextId = 0;
	}
	
	
	
	/**
	 * Adds a player to the game.
	 */
	
	public synchronized boolean addPlayer(Client client, Race race) {
		// Check if there is room for another player.
		if (players.size() >= 6 || started) {
			// No more room.
			return false;
		}
		else {
			// Enough room for another player. Before we can create a player object, we need to retrieve the
			// player's race-specific effect card stack and shuffle it.
			List<EffectCard> effectCardStack = cardLoader.getCards(race);
			Collections.shuffle(effectCardStack);
						
			// Add the player to the player map.
			RemotePlayer player = new RemotePlayer(client, effectCardStack,
					new Player(nextId++, (String)client.attribute("name"), race));
			players.put(client, player);
			
			return true;
		}
	}
	
	
	
	/**
	 * Indicates that the player has left the game.
	 */
	
	public synchronized void removePlayer(Client client) {
		if (started) {
			players.get(client).inactive(true);
			
			boolean allInactive = true;
			for (RemotePlayer player : players.values()) {
				allInactive = allInactive & player.inactive();
			}
			
			if (allInactive) {
				players.clear();
				started = false;
				nextId = 0;
			}
		}
		else {
			players.remove(client);
		}
	}
	
	
	
	/**
	 * Starts the game.
	 */
	
	public synchronized boolean start() {
		if (started) {
			return false;
		}
		else {
			started = true;
			
			long commonSeed = System.currentTimeMillis();
			
			// Create an array of all players.
			Player[] playerArray = new Player[players.size()];
			for (RemotePlayer player : players.values()) {
				playerArray[player.player().id()] = player.player();
			}
			
			// Send a message to all sessions indicating that the game has started. The message carries
			// necessary information, like the names and races of all players and a seed for all randomness
			// that will occur during the game.
			for (Client client : players.keySet()) {
				RemotePlayer player = players.get(client);
				client.send(new GameStartedMessage(player.player().id(), playerArray, commonSeed));
				player.initHandAndNotifyPlayer();
			}
			
			reinforcementStack = new ReinforcementStack();
			
			// Create the starting stack and the reinforcement stack and shuffle them.
			List<FieldCard> startingStack = new LinkedList<FieldCard>();
			List<FieldCard> cardsForReinforcementStack = new LinkedList<FieldCard>();
			for (RemotePlayer player : players.values()) {
				// Add the animals to the starting stack.
				for (byte i = 0; i < STARTINGSTACK_ANIMALS_PER_PLAYER; i++) {
					FieldCard fieldCard = new FieldCard(player.player().race().animalType());
					fieldCard.player(player.player());
					startingStack.add(fieldCard);
				}
				
				// Add the animals and empty fields to the reinforcement stack.
				for (byte i = 0; i < REINFORCEMENTSTACK_ANIMALS_PER_PLAYER; i++) {
					FieldCard fieldCard = new FieldCard(player.player().race().animalType());
					fieldCard.player(player.player());
					cardsForReinforcementStack.add(fieldCard);
				}
				for (byte i = 0; i < REINFORCEMENTSTACK_EMPTY_PER_PLAYER; i++) {
					cardsForReinforcementStack.add(new FieldCard(FieldCard.Type.EMPTY));
				}
			}
			Collections.shuffle(startingStack);
			reinforcementStack.addCardsAndShuffle(cardsForReinforcementStack);
			
			// Add emtpy fields at the bottom of the stack.
			for (byte i = 0; i < REINFORCEMENTSTACK_ADDED_EMPTY; i++) {
				reinforcementStack.addAtBottom(new FieldCard(FieldCard.Type.EMPTY));
			}
			
			// Initialize the play area.
			playArea = new PlayArea(playerArray, commonSeed, this);
			
			// Draw cards from the starting and reinforcment stack until the play area is initialized.
			for (byte i = 0; i < 9; i++) {
				FieldCard card;
				
				// Draw the card from the right stack.
				if (!startingStack.isEmpty()) {
					card = startingStack.remove(0);
				}
				else {
					card = reinforcementStack.getCard();
				}
				
				// Notify the local play area which card was drawn from the stack.
				playArea.drawnCardFromStack(card);
				
				// Send the drawn card to the clients.
				for (Client client : players.keySet()) {
					CardFromReinforcementStackMessage message = new CardFromReinforcementStackMessage(card);
					client.send(message);
				}
			}
			
			// If the starting stack is not emtpy, we put the cards it contains on the reinforcement stack.
			while (!startingStack.isEmpty()) {
				reinforcementStack.addCardOnTop(startingStack.remove(startingStack.size() - 1));
			}
			
			return true;
		}
	}
	
	
	
	/**
	 * Player dropped a card.
	 */
	
	public synchronized boolean playerDroppedCard(Client client, byte index) {
		RemotePlayer player = players.get(client);
		if (playArea.currentTurnPlayer() != player.player()) {
			return false;
		}
		else {
			if (!player.drawCardAndNotifyPlayer(index)) {
				// drawCardAndNotifyPlayer returned false, that means we have no more cards to draw. The game
				// ends.
				for (Client client2 : players.keySet()) {
					client2.send(new GameEndsMessage());
				}
				players.clear();
				started = false;
				nextId = 0;
			}
			playArea.turnChange();
			for (Client client2 : players.keySet()) {
				client2.send(new TurnChangeMessage());
			}
			return true;
		}
	}
	
	
	
	/**
	 * A player played an effect card.
	 */
	
	public synchronized boolean playerPlayedCard(Client client, byte slotId,
			EffectCard.Target... targets) {
		RemotePlayer player = players.get(client);
		if (playArea.currentTurnPlayer() != player.player()) {
			return false;
		}
		else {
			EffectCard card = player.hand(slotId);
			if (!card.isTargetValid(playArea, targets)) {
				return false;
			}
			
			if (!player.drawCardAndNotifyPlayer(slotId)) {
				// drawCardAndNotifyPlayer returned false, that means we have no more cards to draw. The game
				// ends.
				for (Client client2 : players.keySet()) {
					client2.send(new GameEndsMessage());
				}
				players.clear();
				started = false;
				nextId = 0;
				
				return true;
			}
			
			for (Client client2 : players.keySet()) {
				client2.send(new EffectCardPlayedMessage(card, targets));
			}
			card.play(playArea, targets);
			
			// The card has been played. Check if there are animals left in the game.
			boolean animalsLeft = false;
			for (RemotePlayer player2 : players.values()) {
				animalsLeft = animalsLeft || player2.player().animalsLeft() != 0;
			}
			if (animalsLeft) {
				for (Client client2 : players.keySet()) {
					client2.send(new TurnChangeMessage());
				}
				playArea.turnChange();
				
				return true;
			}
			else {
				for (Client client2 : players.keySet()) {
					client2.send(new GameEndsMessage());
				}
				players.clear();
				started = false;
				nextId = 0;
				
				return true;
			}
		}
	}
	
	
	
	/**
	 * Called by the play area when a new card is needed from the reinforcement stack.
	 */
	
	public void newCardNeeded() {
		FieldCard card = null;
		card = reinforcementStack.getCard();
		
		playArea.drawnCardFromStack(card);
		for (Client client : players.keySet()) {
			client.send(new CardFromReinforcementStackMessage(card));
		}
	}
}
